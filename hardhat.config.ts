import '@matterlabs/hardhat-zksync-deploy';
import '@matterlabs/hardhat-zksync-solc';
import '@matterlabs/hardhat-zksync-verify';
import '@nomiclabs/hardhat-ethers';

module.exports = {
  zksolc: {
    version: "1.3.5",
    settings: {
      optimizer: {
        enabled: true, // optional. True by default
        mode: '3' // optional. 3 by default, z to optimize bytecode size
      }
    }
  },
  defaultNetwork: 'zkt',

  networks: {
    goerli: {
      url: 'https://goerli.infura.io/v3/d5765f6fa98d475fad2980c59edc1db7', // you can use either the URL of the Ethereum Web3 RPC, or the identifier of the network (e.g. `mainnet` or `rinkeby`)
    },
    zkt: {
      url: 'https://zksync2-testnet.zksync.dev',
      ethNetwork: "goerli", // Can also be the RPC URL of the network (e.g. `https://goerli.infura.io/v3/<API_KEY>`)
      zksync: true,
    },
  },
  solidity: {
    compilers: [
      {
        version: "0.8.10",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
    ],
  },
  etherscan: {
    apiKey: process.env.API_KEY,
  },
};
