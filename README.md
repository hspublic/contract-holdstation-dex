# HoldStation Trading Contracts

A brief description of what this project does and who it's for

## Deployment

To compile this project run

```bash
  npx hardhat compile
```

To deploy contract run

```bash
  npx hardhat deploy-zksync --script [deploy-script].ts
```

## Note

Script is executed in deploy folder only, so move all to there
