// SPDX-License-Identifier: MIT
import "@openzeppelin/contracts/proxy/transparent/TransparentUpgradeableProxy.sol";
pragma solidity 0.8.10;

/**
 * ProxyPattern is defined to generate the compile version of TransparentUpgradeableProxy.sol
 * TransparentUpgradeableProxy is used to deployed transparent proxies.
 *
 *  */
contract ProxyPattern is TransparentUpgradeableProxy {
  constructor(address _logic, address admin_, bytes memory _data) TransparentUpgradeableProxy(_logic, admin_, _data) {}
}
