// SPDX-License-Identifier: MIT
import "@openzeppelin/contracts/proxy/transparent/ProxyAdmin.sol";
pragma solidity 0.8.10;

contract ProxyAdminPattern is ProxyAdmin {}
