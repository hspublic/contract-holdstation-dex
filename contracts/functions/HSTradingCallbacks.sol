// SPDX-License-Identifier: MIT
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../interfaces/StorageInterfaceV5.sol";
import "../interfaces/HSPairInfosInterfaceV6.sol";
import "../interfaces/HSReferralsInterfaceV6_2.sol";
import "../interfaces/HSStakingInterfaceV6_2.sol";
import "../interfaces/AggregatorInterfaceV6.sol";
import "../interfaces/IHSAgency.sol";
pragma solidity 0.8.10;

contract HSTradingCallbacks is Initializable {
    // Contracts (constant)
    StorageInterfaceV5 public storageT;
    NftRewardsInterfaceV6 public nftRewards;
    HSPairInfosInterfaceV6 public pairInfos;
    HSReferralsInterfaceV6_2 public referrals;
    HSStakingInterfaceV6_2 public staking;

    // Params (constant)
    uint256 constant PRECISION = 1e10; // 10 decimals

    uint256 constant MAX_SL_P = 75; // -75% PNL
    uint256 constant MAX_GAIN_P = 900; // 900% PnL (10x)

    // Params (adjustable)
    uint256 public usdcVaultFeeP; // % of closing fee going to USDC vault (eg. 40)
    uint256 public lpFeeP; // % of closing fee going to HS/USDC LPs (eg. 20)
    uint256 public sssFeeP; // % of closing fee going to HS staking (eg. 40)

    // State
    bool public isPaused; // Prevent opening new trades
    bool public isDone; // Prevent any interaction with the contract

    // Custom data types
    struct AggregatorAnswer {
        uint256 orderId;
        uint256 price;
        uint256 spreadP;
    }

    // Useful to avoid stack too deep errors
    struct Values {
        uint256 posUsdc;
        uint256 levPosUsdc;
        uint256 tokenPriceUsdc;
        int256 profitP;
        uint256 price;
        uint256 liqPrice;
        uint256 usdcSentToTrader;
        uint256 reward1;
        uint256 reward2;
        uint256 reward3;
    }

    // Events
    //topic: 0x2739a12dffae5d66bd9e126a286078ed771840f2288f0afa5709ce38c3330997
    event MarketExecuted(
        uint256 indexed orderId,
        StorageInterfaceV5.Trade t,
        bool open,
        uint256 price,
        uint256 priceImpactP,
        uint256 positionSizeUsdc,
        int256 percentProfit,
        uint256 usdcSentToTrader
    );
    //topic: 0x165b0f8d6347f7ebe92729625b03ace41aeea8fd7ebf640f89f2593ab0db63d1, not move up index range
    event LimitExecuted(
        uint256 indexed orderId,
        uint256 limitIndex,
        StorageInterfaceV5.Trade t,
        address indexed nftHolder,
        StorageInterfaceV5.LimitOrder orderType,
        uint256 price,
        uint256 priceImpactP,
        uint256 positionSizeUsdc,
        int256 percentProfit,
        uint256 usdcSentToTrader
    );

    event MarketOpenCanceled(
        uint256 indexed orderId,
        address indexed trader,
        uint256 indexed pairIndex
    );
    event MarketCloseCanceled(
        uint256 indexed orderId,
        address indexed trader,
        uint256 indexed pairIndex,
        uint256 index
    );

    event SlUpdated(
        uint256 indexed orderId,
        address indexed trader,
        uint256 indexed pairIndex,
        uint256 index,
        uint256 newSl
    );
    event SlCanceled(
        uint256 indexed orderId,
        address indexed trader,
        uint256 indexed pairIndex,
        uint256 index
    );

    event ClosingFeeSharesPUpdated(
        uint256 usdcVaultFeeP,
        uint256 lpFeeP,
        uint256 sssFeeP
    );

    event Pause(bool paused);
    event Done(bool done);

    event DevGovFeeCharged(address indexed trader, uint256 valueUsdc);
    event ReferralFeeCharged(address indexed trader, uint256 valueUsdc);
    event NftBotFeeCharged(address indexed trader, uint256 valueUsdc);
    event SssFeeCharged(address indexed trader, uint256 valueUsdc);
    event UsdcVaultFeeCharged(address indexed trader, uint256 valueUsdc);
    event LpFeeCharged(address indexed trader, uint256 valueUsdc);
    //0f536026a609ee2c02b96e7161b8d8ffd7a46d368c4b227a927334e48c32712d
    event CancelReason(
        uint256 price,
        uint256 currentInterestUsdc,
        uint256 slippage,
        uint256 wantedPrice,
        uint256 priceImpactP,
        uint256 priceAfterImpact,
        bool checkLimit
    );
    event AgencyFeeCharged(address indexed trader, uint256 valueUsdc);
    struct OpenFee {
        uint botFee;
        uint standardRefFee;
        uint agencyFee;
        uint stakingReward;
        uint govFee;
    }

    function initialize(
        StorageInterfaceV5 _storageT,
        NftRewardsInterfaceV6 _nftRewards,
        HSPairInfosInterfaceV6 _pairInfos,
        HSReferralsInterfaceV6_2 _referrals,
        HSStakingInterfaceV6_2 _staking,
        address vaultToApprove,
        uint256 _usdcVaultFeeP,
        uint256 _lpFeeP,
        uint256 _sssFeeP
    ) external initializer {
        require(
            address(_storageT) != address(0) &&
                address(_nftRewards) != address(0) &&
                address(_pairInfos) != address(0) &&
                address(_referrals) != address(0) &&
                address(_staking) != address(0) &&
                _usdcVaultFeeP + _lpFeeP + _sssFeeP == 100,
            "WRONG_PARAMS"
        );

        storageT = _storageT;
        nftRewards = _nftRewards;
        pairInfos = _pairInfos;
        referrals = _referrals;
        staking = _staking;

        usdcVaultFeeP = _usdcVaultFeeP;
        lpFeeP = _lpFeeP;
        sssFeeP = _sssFeeP;

        storageT.usdc().approve(address(staking), type(uint256).max);
        storageT.usdc().approve(vaultToApprove, type(uint256).max);
    }

    function initializeV2(
        HSStakingInterfaceV6_2 _staking
    ) external reinitializer(2) {
        staking = _staking;
        storageT.usdc().approve(address(staking), type(uint256).max);
    }

    // Modifiers
    modifier onlyGov() {
        require(msg.sender == storageT.gov(), "GOV_ONLY");
        _;
    }
    modifier onlyPriceAggregator() {
        require(
            msg.sender == address(storageT.priceAggregator()),
            "AGGREGATOR_ONLY"
        );
        _;
    }
    modifier notDone() {
        require(!isDone, "DONE");
        _;
    }

    // Manage params
    function setClosingFeeSharesP(
        uint256 _usdcVaultFeeP,
        uint256 _lpFeeP,
        uint256 _sssFeeP
    ) external onlyGov {
        require(_usdcVaultFeeP + _lpFeeP + _sssFeeP == 100, "SUM_NOT_100");

        usdcVaultFeeP = _usdcVaultFeeP;
        lpFeeP = _lpFeeP;
        sssFeeP = _sssFeeP;

        emit ClosingFeeSharesPUpdated(_usdcVaultFeeP, _lpFeeP, _sssFeeP);
    }

    // Manage state
    function pause() external onlyGov {
        isPaused = !isPaused;
        emit Pause(isPaused);
    }

    function done() external onlyGov {
        isDone = !isDone;
        emit Done(isDone);
    }

    // Callbacks
    function openTradeMarketCallback(
        AggregatorAnswer memory a
    ) external onlyPriceAggregator notDone {
        StorageInterfaceV5.PendingMarketOrder memory o = storageT
            .reqID_pendingMarketOrder(a.orderId);

        if (o.block == 0) {
            return;
        }

        StorageInterfaceV5.Trade memory t = o.trade;

        (uint256 priceImpactP, uint256 priceAfterImpact) = pairInfos
            .getTradePriceImpact(
                marketExecutionPrice(
                    a.price,
                    a.spreadP,
                    o.spreadReductionP,
                    t.buy
                ),
                t.pairIndex,
                t.buy,
                t.positionSizeUsdc * t.leverage
            );

        t.openPrice = priceAfterImpact;

        uint256 maxSlippage = (o.wantedPrice * o.slippageP) / 100 / PRECISION;
        if (
            isPaused ||
            a.price == 0 ||
            (
                t.buy
                    ? t.openPrice > o.wantedPrice + maxSlippage
                    : t.openPrice < o.wantedPrice - maxSlippage
            ) ||
            (t.tp > 0 && (t.buy ? t.openPrice >= t.tp : t.openPrice <= t.tp)) ||
            (t.sl > 0 && (t.buy ? t.openPrice <= t.sl : t.openPrice >= t.sl)) ||
            !withinExposureLimits(
                t.pairIndex,
                t.buy,
                t.positionSizeUsdc,
                t.leverage
            ) ||
            priceImpactP * t.leverage > pairInfos.maxNegativePnlOnOpenP()
        ) {
            uint256 devGovFeesUsdc = storageT.handleGoldGovFees(
                t.pairIndex,
                t.positionSizeUsdc * t.leverage,
                0,
                t.trader,
                true
            );

            storageT.transferUsdc(
                address(storageT),
                t.trader,
                t.positionSizeUsdc - devGovFeesUsdc
            );

            emit DevGovFeeCharged(t.trader, devGovFeesUsdc);

            emit MarketOpenCanceled(a.orderId, t.trader, t.pairIndex);
            emit CancelReason(
                a.price,
                storageT.openInterestUsdc(t.pairIndex, t.buy ? 0 : 1),
                o.slippageP,
                o.wantedPrice,
                priceImpactP,
                priceAfterImpact,
                withinExposureLimits(
                    t.pairIndex,
                    t.buy,
                    t.positionSizeUsdc,
                    t.leverage
                )
            );
        } else {
            (StorageInterfaceV5.Trade memory finalTrade, ) = registerTrade(
                t,
                1500,
                0
            );

            emit MarketExecuted(
                a.orderId,
                finalTrade,
                true,
                finalTrade.openPrice,
                priceImpactP,
                finalTrade.initialPosToken,
                0,
                0
            );
        }

        storageT.unregisterPendingMarketOrder(a.orderId, true);
    }

    function closeTradeMarketCallback(
        AggregatorAnswer memory a
    ) external onlyPriceAggregator notDone {
        StorageInterfaceV5.PendingMarketOrder memory o = storageT
            .reqID_pendingMarketOrder(a.orderId);

        if (o.block == 0) {
            return;
        }

        StorageInterfaceV5.Trade memory t = storageT.openTrades(
            o.trade.trader,
            o.trade.pairIndex,
            o.trade.index
        );

        if (t.leverage > 0) {
            StorageInterfaceV5.TradeInfo memory i = storageT.openTradesInfo(
                t.trader,
                t.pairIndex,
                t.index
            );
            Values memory v;

            v.levPosUsdc = t.initialPosToken * t.leverage;

            if (a.price == 0) {
                // Dev / gov rewards to pay for oracle cost
                // Charge in USDC if collateral in storage or token if collateral in vault
                v.reward1 = storageT.handleGoldGovFees(
                    t.pairIndex,
                    v.levPosUsdc,
                    0,
                    t.trader,
                    true
                );

                t.initialPosToken -= v.reward1;
                storageT.updateTrade(t);

                emit DevGovFeeCharged(t.trader, v.reward1);

                emit MarketCloseCanceled(
                    a.orderId,
                    t.trader,
                    t.pairIndex,
                    t.index
                );
            } else {
                v.profitP = currentPercentProfit(
                    t.openPrice,
                    a.price,
                    t.buy,
                    t.leverage
                );
                v.posUsdc = v.levPosUsdc / t.leverage;
                v.usdcSentToTrader = unregisterTrade(
                    t,
                    true,
                    v.profitP,
                    v.posUsdc,
                    i.openInterestUsdc / t.leverage,
                    (v.levPosUsdc *
                        pairStorageContract().pairCloseFeeP(t.pairIndex)) /
                        100 /
                        PRECISION,
                    (v.levPosUsdc *
                        pairStorageContract().pairNftLimitOrderFeeP(
                            t.pairIndex
                        )) /
                        100 /
                        PRECISION,
                    PRECISION
                );

                emit MarketExecuted(
                    a.orderId,
                    t,
                    false,
                    a.price,
                    0,
                    v.posUsdc,
                    v.profitP,
                    v.usdcSentToTrader
                );
            }
        }

        storageT.unregisterPendingMarketOrder(a.orderId, false);
    }

    function executeNftOpenOrderCallback(
        AggregatorAnswer memory a
    ) external onlyPriceAggregator notDone {
        StorageInterfaceV5.PendingNftOrder memory n = storageT
            .reqID_pendingNftOrder(a.orderId);

        if (
            !isPaused &&
            a.price > 0 &&
            storageT.hasOpenLimitOrder(n.trader, n.pairIndex, n.index) &&
            block.number >=
            storageT.nftLastSuccess(n.nftId) + storageT.nftSuccessTimelock()
        ) {
            StorageInterfaceV5.OpenLimitOrder memory o = storageT
                .getOpenLimitOrder(n.trader, n.pairIndex, n.index);

            NftRewardsInterfaceV6.OpenLimitOrderType t = nftRewards
                .openLimitOrderTypes(n.trader, n.pairIndex, n.index);

            (uint256 priceImpactP, uint256 priceAfterImpact) = pairInfos
                .getTradePriceImpact(
                    marketExecutionPrice(
                        a.price,
                        a.spreadP,
                        o.spreadReductionP,
                        o.orderInfo.buy
                    ),
                    o.orderInfo.pairIndex,
                    o.orderInfo.buy,
                    o.orderInfo.positionSize * o.orderInfo.leverage
                );

            a.price = priceAfterImpact;
            if (
                (
                    t == NftRewardsInterfaceV6.OpenLimitOrderType.LEGACY
                        ? (a.price >= o.orderInfo.minPrice &&
                            a.price <= o.orderInfo.maxPrice)
                        : t == NftRewardsInterfaceV6.OpenLimitOrderType.REVERSAL
                        ? (
                            o.orderInfo.buy
                                ? a.price <= o.orderInfo.maxPrice
                                : a.price >= o.orderInfo.minPrice
                        )
                        : (
                            o.orderInfo.buy
                                ? a.price >= o.orderInfo.minPrice
                                : a.price <= o.orderInfo.maxPrice
                        )
                ) &&
                withinExposureLimits(
                    o.orderInfo.pairIndex,
                    o.orderInfo.buy,
                    o.orderInfo.positionSize,
                    o.orderInfo.leverage
                ) &&
                priceImpactP * o.orderInfo.leverage <=
                pairInfos.maxNegativePnlOnOpenP()
            ) {
                (StorageInterfaceV5.Trade memory finalTrade, ) = registerTrade(
                    StorageInterfaceV5.Trade(
                        o.trader,
                        o.orderInfo.pairIndex,
                        0,
                        0,
                        o.orderInfo.positionSize,
                        t == NftRewardsInterfaceV6.OpenLimitOrderType.REVERSAL
                            ? o.orderInfo.maxPrice // o.minPrice = o.maxPrice in that case
                            : a.price,
                        o.orderInfo.buy,
                        o.orderInfo.leverage,
                        o.orderInfo.tp,
                        o.orderInfo.sl
                    ),
                    n.nftId,
                    n.index
                );

                storageT.unregisterOpenLimitOrder(
                    o.trader,
                    o.orderInfo.pairIndex,
                    o.index
                );

                emit LimitExecuted(
                    a.orderId,
                    n.index,
                    finalTrade,
                    n.nftHolder,
                    StorageInterfaceV5.LimitOrder.OPEN,
                    finalTrade.openPrice,
                    priceImpactP,
                    finalTrade.initialPosToken,
                    0,
                    0
                );
            } else {
                emit CancelReason(
                    a.price,
                    o.orderInfo.tp,
                    o.orderInfo.sl,
                    uint256(t),
                    withinExposureLimits(
                        o.orderInfo.pairIndex,
                        o.orderInfo.buy,
                        o.orderInfo.positionSize,
                        o.orderInfo.leverage
                    )
                        ? 1
                        : 0,
                    6886,
                    o.orderInfo.buy
                );
            }
        }
        nftRewards.unregisterTrigger(
            NftRewardsInterfaceV6.TriggeredLimitId(
                n.trader,
                n.pairIndex,
                n.index,
                n.orderType
            )
        );
        storageT.unregisterPendingNftOrder(a.orderId);
    }

    function executeNftCloseOrderCallback(
        AggregatorAnswer memory a
    ) external onlyPriceAggregator notDone {
        StorageInterfaceV5.PendingNftOrder memory o = storageT
            .reqID_pendingNftOrder(a.orderId);

        StorageInterfaceV5.Trade memory t = storageT.openTrades(
            o.trader,
            o.pairIndex,
            o.index
        );
        if (
            a.price > 0 &&
            t.leverage > 0 &&
            block.number >=
            storageT.nftLastSuccess(o.nftId) + storageT.nftSuccessTimelock()
        ) {
            StorageInterfaceV5.TradeInfo memory i = storageT.openTradesInfo(
                t.trader,
                t.pairIndex,
                t.index
            );
            Values memory v;

            v.price = a.price;

            v.profitP = currentPercentProfit(
                t.openPrice,
                v.price,
                t.buy,
                t.leverage
            );
            v.levPosUsdc = t.initialPosToken * t.leverage;
            v.posUsdc = v.levPosUsdc / t.leverage;

            if (o.orderType == StorageInterfaceV5.LimitOrder.LIQ) {
                v.liqPrice = pairInfos.getTradeLiquidationPrice(
                    t.trader,
                    t.pairIndex,
                    t.index,
                    t.openPrice,
                    t.buy,
                    v.posUsdc,
                    t.leverage
                );

                // NFT reward in USDC
                v.reward1 = (
                    t.buy ? a.price <= v.liqPrice : a.price >= v.liqPrice
                )
                    ? (v.posUsdc * 5) / 100
                    : 0;
            } else {
                // NFT reward in USDC
                v.reward1 = ((o.orderType == StorageInterfaceV5.LimitOrder.TP &&
                    t.tp > 0 &&
                    (t.buy ? a.price >= t.tp : a.price <= t.tp)) ||
                    (o.orderType == StorageInterfaceV5.LimitOrder.SL &&
                        t.sl > 0 &&
                        (t.buy ? a.price <= t.sl : a.price >= t.sl)))
                    ? (v.levPosUsdc *
                        pairStorageContract().pairNftLimitOrderFeeP(
                            t.pairIndex
                        )) /
                        100 /
                        PRECISION
                    : 0;
            }
            // If can be triggered
            if (v.reward1 > 0) {
                v.usdcSentToTrader = unregisterTrade(
                    t,
                    false,
                    v.profitP,
                    v.posUsdc,
                    i.openInterestUsdc / t.leverage,
                    o.orderType == StorageInterfaceV5.LimitOrder.LIQ
                        ? v.reward1
                        : (v.levPosUsdc *
                            pairStorageContract().pairCloseFeeP(t.pairIndex)) /
                            100 /
                            PRECISION,
                    v.reward1,
                    PRECISION
                );

                // Convert NFT bot fee from USDC to token value
                v.reward2 = v.reward1;

                nftRewards.distributeNftReward(
                    NftRewardsInterfaceV6.TriggeredLimitId(
                        o.trader,
                        o.pairIndex,
                        o.index,
                        o.orderType
                    ),
                    v.reward2
                );

                storageT.increaseNftRewards(o.nftId, v.reward2);

                emit NftBotFeeCharged(t.trader, v.reward1);

                emit LimitExecuted(
                    a.orderId,
                    o.index,
                    t,
                    o.nftHolder,
                    o.orderType,
                    v.price,
                    0,
                    v.posUsdc,
                    v.profitP,
                    v.usdcSentToTrader
                );
            } else {
                emit CancelReason(
                    a.price,
                    t.sl,
                    t.tp,
                    uint256(o.orderType),
                    v.liqPrice,
                    6868,
                    t.buy
                );
            }
        }

        nftRewards.unregisterTrigger(
            NftRewardsInterfaceV6.TriggeredLimitId(
                o.trader,
                o.pairIndex,
                o.index,
                o.orderType
            )
        );

        storageT.unregisterPendingNftOrder(a.orderId);
    }

    // Shared code between market & limit callbacks
    function registerTrade(
        StorageInterfaceV5.Trade memory trade,
        uint256 nftId,
        uint256 limitIndex
    ) private returns (StorageInterfaceV5.Trade memory, uint256) {
        OpenFee memory f = calculateOpenFee(trade, nftId);

        // 1. Charge referral fee (if applicable)
        if (f.standardRefFee > 0) {
            trade.positionSizeUsdc -= f.standardRefFee;
            emit ReferralFeeCharged(trade.trader, f.standardRefFee);
        } else if (f.agencyFee > 0) {
            // 2. Charge agency fee (if applicable)
            trade.positionSizeUsdc -= f.agencyFee;
            emit AgencyFeeCharged(trade.trader, f.agencyFee);
        }
        // 2. Charge bot fee (if applicable)
        if (nftId < 1500) {
            nftRewards.distributeNftReward(
                NftRewardsInterfaceV6.TriggeredLimitId(
                    trade.trader,
                    trade.pairIndex,
                    limitIndex,
                    StorageInterfaceV5.LimitOrder.OPEN
                ),
                f.botFee
            );
            storageT.increaseNftRewards(nftId, f.botFee);
            trade.positionSizeUsdc -= f.botFee;
            emit NftBotFeeCharged(trade.trader, f.botFee);
        } else {
            //distribute staking reward
            distributeStakingReward(trade.trader, f.stakingReward);
            trade.positionSizeUsdc -= f.stakingReward;
        }

        // 2. Charge opening fee - referral fee (if applicable)
        storageT.chargeGovFees(f.govFee, true);
        trade.positionSizeUsdc -= f.govFee;
        emit DevGovFeeCharged(trade.trader, f.govFee);

        // 4. Set trade final details
        trade.index = storageT.firstEmptyTradeIndex(
            trade.trader,
            trade.pairIndex
        );
        trade.initialPosToken = trade.positionSizeUsdc;

        trade.tp = correctTp(
            trade.openPrice,
            trade.leverage,
            trade.tp,
            trade.buy
        );
        trade.sl = correctSl(
            trade.openPrice,
            trade.leverage,
            trade.sl,
            trade.buy
        );

        // 5. Call other contracts
        pairInfos.storeTradeInitialAccFees(
            trade.trader,
            trade.pairIndex,
            trade.index,
            trade.buy
        );
        pairStorageContract().updateGroupCollateral(
            trade.pairIndex,
            trade.positionSizeUsdc,
            trade.buy,
            true
        );

        // 6. Store final trade in storage contract
        storageT.storeTrade(
            trade,
            StorageInterfaceV5.TradeInfo(
                0,
                PRECISION,
                trade.positionSizeUsdc * trade.leverage,
                0,
                0,
                false
            )
        );

        return (trade, PRECISION);
    }

    function unregisterTrade(
        StorageInterfaceV5.Trade memory trade,
        bool marketOrder,
        int256 percentProfit, // PRECISION
        uint256 currentUsdcPos, // 1e18
        uint256 initialUsdcPos, // 1e18
        uint256 closingFeeUsdc, // 1e18
        uint256 nftFeeUsdc, // 1e18 (= SSS reward if market order)
        uint256 // PRECISION
    ) private returns (uint256 usdcSentToTrader) {
        // 1. Calculate net PnL (after all closing fees)
        // 1. Calculate net PnL (after all closing fees)
        usdcSentToTrader = pairInfos.getTradeValue(
            trade.trader,
            trade.pairIndex,
            trade.index,
            trade.buy,
            currentUsdcPos,
            trade.leverage,
            percentProfit,
            closingFeeUsdc + nftFeeUsdc
        );

        Values memory v;

        // 2. LP reward
        if (lpFeeP > 0) {
            v.reward1 = (closingFeeUsdc * lpFeeP) / 100;
            emit LpFeeCharged(trade.trader, v.reward1);
        }

        // 3.1 If collateral in storage (opened after update)
        uint256 usdcKeepInStorage = !marketOrder ? nftFeeUsdc : 0;
        if (trade.positionSizeUsdc > 0) {
            // 3.1.1 vault reward
            v.reward2 = (closingFeeUsdc * usdcVaultFeeP) / 100;

            storageT.transferUsdc(address(storageT), address(this), v.reward2);
            storageT.vault().distributeReward(v.reward2);
            emit UsdcVaultFeeCharged(trade.trader, v.reward2);

            // 3.1.2 SSS reward
            v.reward3 = marketOrder
                ? nftFeeUsdc + (closingFeeUsdc * sssFeeP) / 100
                : (closingFeeUsdc * sssFeeP) / 100;
            //chargeGovFees to gov at v1, after integrate staking, fee will distribute to staking
            //storageT.chargeGovFees(v.reward3, true); v1
            distributeStakingReward(trade.trader, v.reward3);

            // 3.1.3 Take from vault if winning trade or send to vault if losing trade
            uint256 usdcLeftInStorage = currentUsdcPos -
                v.reward3 -
                v.reward2 -
                usdcKeepInStorage;

            if (usdcSentToTrader > usdcLeftInStorage) {
                storageT.vault().sendAssets(
                    usdcSentToTrader - usdcLeftInStorage,
                    trade.trader
                );
                storageT.transferUsdc(
                    address(storageT),
                    trade.trader,
                    usdcLeftInStorage
                );
            } else {
                if (usdcLeftInStorage - usdcSentToTrader > 0) {
                    sendToVault(
                        usdcLeftInStorage - usdcSentToTrader,
                        trade.trader
                    );
                }
                storageT.transferUsdc(
                    address(storageT),
                    trade.trader,
                    usdcSentToTrader
                );
            }
        } else {
            storageT.vault().sendAssets(usdcSentToTrader, trade.trader);
        }

        // 4. Calls to other contracts
        pairStorageContract().updateGroupCollateral(
            trade.pairIndex,
            initialUsdcPos,
            trade.buy,
            false
        );

        // 5. Unregister trade
        storageT.unregisterTrade(trade.trader, trade.pairIndex, trade.index);
    }

    // Utils
    function withinExposureLimits(
        uint256 pairIndex,
        bool buy,
        uint256 positionSizeUsdc,
        uint256 leverage
    ) private view returns (bool) {
        PairsStorageInterfaceV6 pairsStored = pairStorageContract();
        return
            storageT.openInterestUsdc(pairIndex, buy ? 0 : 1) +
                positionSizeUsdc *
                leverage <=
            storageT.openInterestUsdc(pairIndex, 2) &&
            pairsStored.groupCollateral(pairIndex, buy) + positionSizeUsdc <=
            pairsStored.groupMaxCollateral(pairIndex);
    }

    function currentPercentProfit(
        uint256 openPrice,
        uint256 currentPrice,
        bool buy,
        uint256 leverage
    ) private pure returns (int256 p) {
        int256 maxPnlP = int256(MAX_GAIN_P) * int256(PRECISION);

        p =
            ((
                buy
                    ? int256(currentPrice) - int256(openPrice)
                    : int256(openPrice) - int256(currentPrice)
            ) *
                100 *
                int256(PRECISION) *
                int256(leverage)) /
            int256(openPrice);

        p = p > maxPnlP ? maxPnlP : p;
    }

    function correctTp(
        uint256 openPrice,
        uint256 leverage,
        uint256 tp,
        bool buy
    ) private pure returns (uint256) {
        if (
            tp == 0 ||
            currentPercentProfit(openPrice, tp, buy, leverage) ==
            int256(MAX_GAIN_P) * int256(PRECISION)
        ) {
            uint256 tpDiff = (openPrice * MAX_GAIN_P) / leverage / 100;

            return
                buy ? openPrice + tpDiff : tpDiff <= openPrice
                    ? openPrice - tpDiff
                    : 0;
        }

        return tp;
    }

    function correctSl(
        uint256 openPrice,
        uint256 leverage,
        uint256 sl,
        bool buy
    ) private pure returns (uint256) {
        if (
            sl > 0 &&
            currentPercentProfit(openPrice, sl, buy, leverage) <
            int256(MAX_SL_P) * int256(PRECISION) * -1
        ) {
            uint256 slDiff = (openPrice * MAX_SL_P) / leverage / 100;

            return buy ? openPrice - slDiff : openPrice + slDiff;
        }

        return sl;
    }

    function marketExecutionPrice(
        uint256 price,
        uint256 spreadP,
        uint256 spreadReductionP,
        bool long
    ) private pure returns (uint256) {
        uint256 priceDiff = (price *
            (spreadP - (spreadP * spreadReductionP) / 100)) /
            100 /
            PRECISION;

        return long ? price + priceDiff : price - priceDiff;
    }

    function distributeStakingReward(address _trader, uint256 _amount) private {
        storageT.transferUsdc(address(storageT), address(this), _amount);
        staking.distributeRewardUsdc(_amount);
        emit SssFeeCharged(_trader, _amount);
    }

    function sendToVault(uint256 amountUsdc, address trader) private {
        storageT.transferUsdc(address(storageT), address(this), amountUsdc);
        storageT.vault().receiveAssets(amountUsdc, trader);
    }

    function pairStorageContract()
        private
        view
        returns (PairsStorageInterfaceV6 pairsStored)
    {
        return aggregatorContract().pairsStorage();
    }

    function aggregatorContract()
        private
        view
        returns (AggregatorInterfaceV6 aggregator)
    {
        return storageT.priceAggregator();
    }

    function agencyContract() private view returns (IHSAgency aggregator) {
        return storageT.hsAgency();
    }

    /*

  |   User           | Type         | Vault | Bot | Gov   | Gold | Agency | Referrer | HOLD Token | Total  | Measure           |
  |------------------|--------------|-------|-----|-------|------|--------|----------|------------|--------|-------------------|
  | Normal (no ref)  | Market Open  | -     | -   | 60    | -    | -      | -        | 40         |        | openFee           |
  | Normal (no ref)  | Limit Open   | -     | 25  | 75    | -    | -      | -        | -          | 100%   | openFee           |
  | Standard         | Market Open  | -     | -   | 39,75 | -    | -      | 20,25    | 40         | 100%   | openFee           |
  | Standard         | Limit Open   | -     | 25  | 54,75 | -    | -      | 20,25    | -          | 100%   | openFee           |
  | Agency0          | Market Open  | -     | -   | 50    | -    | -      | -        | 40         | 90%    | openFee           |
  | Agency0          | Limit Open   | -     | 25  | 65    | -    | -      | -        | -          | 90%    | openFee           |
  | Agency1          | Market Open  | -     | -   | 10    | -    | 40     | -        | 40         | 90%    | openFee           |
  | Agency1          | Limit Open   | -     | 25  | 25    | -    | 40     | -        | -          | 90%    | openFee           |
  | Agency2,3        | Market Open  | -     | -   | 10    | -    | 10     | 30       | 40         | 90%    | openFee           |
  | Agency2,3        | Limit Open   | -     | 25  | 25    | -    | 10     | 30       | -          | 90%    | openFee           |
  | All user         | Market Close | 37,5  | -   |       | -    | -      | -        | 62,5       | 100%   | closeFee          |
  | All user         | Close TP, SL | 37,5  | 25  |       | -    | -      | -        | 37,5       | 100%   | closeFee          |
  | All user         | Close Liquid | 92,5  | 5   |       | -    | -      | -        | 2,5        | 100%   | loss (collateral) |
  
  The general formula to be configured is 
    -levPos = trade.positionSizeUsdc * trade.leverage
    -openFeeP = pairStorageContract().pairOpenFeeP() = 0.03% => fullFeeF = 0.06%
    -botFeeF = pairStorageContract().pairNftLimitOrderFeeP() = 0.02%
   * Case bot execute (no staking reward):
    -botFee = levPos * botFeeF
    *Has standard ref:
      -refFee = levPos * fullFeeF * refFeeP, refFee = 27% ~ 20/75 config in Referral contract, referrals.distributePotentialReward()
      -govFee = levPos * fullFeeF - refFee
    *Else has agency ref: Measure: 90%
      +If the trader exists in the agency system
        -Base agencyFee = levPos * fullFeeF * 40/75
        -Trader is LVL0 => agencyFee = 0
        -Trader is LVL1 => 40% for LVL0
        -Trader is LVL2/LVL3 => 10% for LVL0, 30% for parent
        -govFee = levPos * fullFeeF * 65/75 - agencyFee
    *No standard ref, no agency ref
      -govFee = levPos * fullFeeF
   * Case market execute (share staking reward):
      -stakingReward1 = levPos * botFeeF
      -stakingReward2 = levPosUsdc * fullFee * (15 / 75)
    *Has standard ref / has agency ref: Calculated like the bot execute case
      (standard ref) => govFee = levPos * fullFeeF - refFee - stakingReward2 
      (Agency ref) => govFee = levPos * fullFeeF * 65/75 - agencyFee - stakingReward2
    *No standard ref, no agency ref
      -govFee = levPos * fullFeeF - stakingReward2
   */
    function calculateOpenFee(
        StorageInterfaceV5.Trade memory trade,
        uint256 nftId
    ) private returns (OpenFee memory) {
        OpenFee memory f;
        uint levPosUsdc = trade.positionSizeUsdc * trade.leverage;
        uint pairOpenFeeP = pairStorageContract().pairOpenFeeP(trade.pairIndex);
        uint fullFee = pairOpenFeeP * 2;
        uint stakingReward2 = 0;
        bool isAgencyRef = false;
        f.standardRefFee = distributeStandardRefFee(
            levPosUsdc,
            pairOpenFeeP,
            trade
        );
        //bot execute
        if (nftId < 1500) {
            //0.02 % fee for nft
            f.botFee =
                (levPosUsdc *
                    pairStorageContract().pairNftLimitOrderFeeP(
                        trade.pairIndex
                    )) /
                100 /
                PRECISION;
        } else {
            //same nft percentage
            uint stakingReward1 = (levPosUsdc *
                pairStorageContract().pairNftLimitOrderFeeP(trade.pairIndex)) /
                100 /
                PRECISION;
            stakingReward2 = (levPosUsdc * fullFee * 15) / 75 / 100 / PRECISION;
            f.stakingReward += (stakingReward1 + stakingReward2);
        }
        if (f.standardRefFee == 0) {
            (f.agencyFee, isAgencyRef) = distributeAgencyFee(
                levPosUsdc,
                fullFee,
                trade
            );
        }
        f.govFee = isAgencyRef
            ? ((levPosUsdc * fullFee * 65) /
                75 /
                100 /
                PRECISION -
                f.agencyFee -
                stakingReward2)
            : ((levPosUsdc * fullFee) /
                100 /
                PRECISION -
                f.standardRefFee -
                stakingReward2);
        return f;
    }

    function distributeStandardRefFee(
        uint levPosUsdc,
        uint pairOpenFeeP,
        StorageInterfaceV5.Trade memory trade
    ) private returns (uint) {
        if (referrals.getTraderReferrer(trade.trader) != address(0)) {
            // Use this variable to store lev pos usdc for dev/gov fees after referral fees
            // and before volumeReferredUsdc increases
            // v.posUsdc = (v.levPosUsdc * (100 * PRECISION - referrals.getPercentOfOpenFeeP(trade.trader))) / 100 / PRECISION;
            // pairOpenFeeP = 0.03 => distributePotentialReward cal 27% ~20/75
            return
                referrals.distributePotentialReward(
                    trade.trader,
                    levPosUsdc,
                    pairOpenFeeP,
                    PRECISION
                );
        }
        return 0;
    }

    function distributeAgencyFee(
        uint levPosUsdc,
        uint fullFee,
        StorageInterfaceV5.Trade memory trade
    ) private returns (uint, bool) {
        //Not in agency, has no root
        if (agencyContract().rootReferrer(trade.trader) == address(0)) {
            return (0, false);
        }
        uint fee = (levPosUsdc * fullFee) / PRECISION / 100;
        return (agencyContract().distributeReward(fee, trade.trader), true);
    }
}
