// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../interfaces/TokenInterfaceV5.sol";
import "../interfaces/AggregatorInterfaceV5.sol";
import "../interfaces/NftInterfaceV5.sol";
import "../interfaces/PausableInterfaceV5.sol";
import "../helpers/ArrayUint256.sol";
import "../interfaces/IHSAgency.sol";

contract HSTradingStorage is Initializable {
  // Constants
  uint256 public constant PRECISION = 1e10;
  bytes32 public constant MINTER_ROLE = 0x9f2df0fed2c77648de5860a4cc508cd0818c85b8b8a1ab4ceeef8d981c8956a6;
  TokenInterfaceV5 public usdc;
  TokenInterfaceV5 public linkErc677;

  // Contracts (updatable)
  AggregatorInterfaceV5 public priceAggregator;
  PausableInterfaceV5 public trading;
  PausableInterfaceV5 public callbacks;
  TokenInterfaceV5 public token;
  NftInterfaceV5[5] public nfts;
  address public vault;
  address public tokenUsdcRouter;

  // Trading variables
  uint256 public maxTradesPerPair;
  uint256 public maxPendingMarketOrders;
  uint256 public nftSuccessTimelock; // 50 blocks
  uint256[5] public spreadReductionsP; // %

  // Gov & dev addresses (updatable)
  address public gov;
  address public goldManager;

  // Gov & dev fees
  uint256 public goldFeesToken; // 1e18
  uint256 public goldFeesUsdc; // 1e6
  uint256 public govFeesToken; // 1e18
  uint256 public govFeesUsdc; // 1e6

  // Stats
  uint256 public tokensBurned; // 1e18
  uint256 public tokensMinted; // 1e18
  uint256 public nftRewards; // 1e18
  uint256 public goldFeeP; //8*1e10/30
  // Enums
  enum LimitOrder {
    TP,
    SL,
    LIQ,
    OPEN
  }

  // Structs
  struct Trader {
    uint256 leverageUnlocked;
    address referral;
    uint256 referralRewardsTotal; // 1e18
  }
  struct Trade {
    address trader;
    uint256 pairIndex;
    uint256 index;
    uint256 initialPosToken; // 1e18
    uint256 positionSizeUsdc; // 1e18
    uint256 openPrice; // PRECISION
    bool buy;
    uint256 leverage;
    uint256 tp; // PRECISION
    uint256 sl; // PRECISION
  }
  struct TradeInfo {
    uint256 tokenId;
    uint256 tokenPriceUsdc; // PRECISION
    uint256 openInterestUsdc; // 1e18
    uint256 tpLastUpdated;
    uint256 slLastUpdated;
    bool beingMarketClosed;
  }
  struct OpenLimitOrder {
    OrderInfo orderInfo;
    address trader;
    uint256 index;
    uint256 spreadReductionP;
    uint256 block;
  }
  struct PendingMarketOrder {
    Trade trade;
    uint256 block;
    uint256 wantedPrice; // PRECISION
    uint256 slippageP; // PRECISION (%)
    uint256 spreadReductionP;
    uint256 tokenId; // index in supportedTokens
  }
  struct PendingNftOrder {
    address nftHolder;
    uint256 nftId;
    address trader;
    uint256 pairIndex;
    uint256 index;
    LimitOrder orderType;
  }

  // Supported tokens to open trades with
  address[] public supportedTokens;

  // User info mapping
  mapping(address => Trader) public traders;

  // Trades mappings
  mapping(address => mapping(uint256 => mapping(uint256 => Trade))) public openTrades;
  mapping(address => mapping(uint256 => mapping(uint256 => TradeInfo))) public openTradesInfo;
  mapping(address => mapping(uint256 => uint256)) public openTradesCount;

  // Limit orders mappings
  mapping(address => mapping(uint256 => mapping(uint256 => uint256))) public openLimitOrderIds;
  mapping(address => mapping(uint256 => uint256)) public openLimitOrdersCount;
  OpenLimitOrder[] public openLimitOrders;

  // Pending orders mappings
  mapping(uint256 => PendingMarketOrder) public reqID_pendingMarketOrder;
  mapping(uint256 => PendingNftOrder) public reqID_pendingNftOrder;
  mapping(address => uint256[]) public pendingOrderIds;
  mapping(address => mapping(uint256 => uint256)) public pendingMarketOpenCount;
  mapping(address => mapping(uint256 => uint256)) public pendingMarketCloseCount;

  // List of open trades & limit orders
  mapping(uint256 => address[]) public pairTraders;
  mapping(address => mapping(uint256 => uint256)) public pairTradersId;

  // Current and max open interests for each pair
  mapping(uint256 => uint256[3]) public openInterestUsdc; // 1e18 [long,short,max]

  // Restrictions & Timelocks
  mapping(uint256 => uint256) public tradesPerBlock;
  mapping(uint256 => uint256) public nftLastSuccess;

  // List of allowed contracts => can update storage + mint/burn tokens
  mapping(address => bool) public isTradingContract;

  // Events
  event SupportedTokenAdded(address a);
  event TradingContractAdded(address a);
  event TradingContractRemoved(address a);
  event AddressUpdated(string name, address a);
  event NftsUpdated(NftInterfaceV5[5] nfts);
  event NumberUpdated(string name, uint256 value);
  event NumberUpdatedPair(string name, uint256 pairIndex, uint256 value);
  event SpreadReductionsUpdated(uint256[5]);

  using ArrayUint256 for uint256[];
  uint256[] public currentPendingOrderIds;

  event OpenInterestExecuted(uint256 pairIndex, bool open, bool long, uint256[3] datas);
  struct OrderInfo {
    uint256 pairIndex;
    uint256 positionSize;
    bool buy;
    uint256 leverage;
    uint256 tp;
    uint256 sl;
    uint256 minPrice;
    uint256 maxPrice;
  }

  IHSAgency public hsAgency;
  mapping(uint => uint) public indexOfPendingOrderIds;

  function initialize(
    TokenInterfaceV5 _usdc,
    TokenInterfaceV5 _token,
    TokenInterfaceV5 _linkErc677,
    NftInterfaceV5[5] memory _nfts
  ) external initializer {
    usdc = _usdc;
    token = _token;
    linkErc677 = _linkErc677;
    nfts = _nfts;
    maxTradesPerPair = 3;
    maxPendingMarketOrders = 5;
    nftSuccessTimelock = 50; // 50 blocks
    spreadReductionsP = [15, 20, 25, 30, 35]; // %
    gov = msg.sender;
    goldManager = msg.sender;
    goldFeeP = 2666666666;
  }

  // Modifiers
  modifier onlyGov() {
    require(msg.sender == gov);
    _;
  }

  modifier onlyTrading() {
    require(isTradingContract[msg.sender] && token.hasRole(MINTER_ROLE, msg.sender));
    _;
  }

  // Manage addresses
  function setGov(address _gov) external onlyGov {
    require(_gov != address(0));
    gov = _gov;
    emit AddressUpdated("gov", _gov);
  }

  function setGoldManager(address _goldManager) external onlyGov {
    require(_goldManager != address(0));
    goldManager = _goldManager;
    emit AddressUpdated("goldManager", _goldManager);
  }

  function setGoldFeeP(uint256 _goldFeeP) external onlyGov {
    //*1e10: Eg: 8/30 * 1e10 = 2666666666
    goldFeeP = _goldFeeP;
  }

  function updateNfts(NftInterfaceV5[5] memory _nfts) external onlyGov {
    require(address(_nfts[0]) != address(0));
    nfts = _nfts;
    emit NftsUpdated(_nfts);
  }

  // Trading + callbacks contracts
  function addTradingContract(address _trading) external onlyGov {
    _addTradingContract(_trading);
  }

  function addTradingContracts(address[] memory _tradings) external onlyGov {
    for (uint i = 0; i < _tradings.length; i++) {
      _addTradingContract(_tradings[i]);
    }
  }

  function _addTradingContract(address _trading) private {
    require(token.hasRole(MINTER_ROLE, _trading), "NOT_MINTER");
    require(_trading != address(0));
    isTradingContract[_trading] = true;
    emit TradingContractAdded(_trading);
  }

  function removeTradingContract(address _trading) external onlyGov {
    require(_trading != address(0));
    isTradingContract[_trading] = false;
    emit TradingContractRemoved(_trading);
  }

  function setPriceAggregator(address _aggregator) external onlyGov {
    require(_aggregator != address(0));
    priceAggregator = AggregatorInterfaceV5(_aggregator);
    emit AddressUpdated("priceAggregator", _aggregator);
  }

  function setVault(address _vault) external onlyGov {
    require(_vault != address(0));
    vault = _vault;
    emit AddressUpdated("vault", _vault);
  }

  function setTrading(address _trading) external onlyGov {
    require(_trading != address(0));
    trading = PausableInterfaceV5(_trading);
    emit AddressUpdated("trading", _trading);
  }

  function setCallbacks(address _callbacks) external onlyGov {
    require(_callbacks != address(0));
    callbacks = PausableInterfaceV5(_callbacks);
    emit AddressUpdated("callbacks", _callbacks);
  }

  function setMaxTradesPerPair(uint256 _maxTradesPerPair) external onlyGov {
    require(_maxTradesPerPair > 0);
    maxTradesPerPair = _maxTradesPerPair;
    emit NumberUpdated("maxTradesPerPair", _maxTradesPerPair);
  }

  function setMaxPendingMarketOrders(uint256 _maxPendingMarketOrders) external onlyGov {
    require(_maxPendingMarketOrders > 0);
    maxPendingMarketOrders = _maxPendingMarketOrders;
    emit NumberUpdated("maxPendingMarketOrders", _maxPendingMarketOrders);
  }

  function setNftSuccessTimelock(uint256 _blocks) external onlyGov {
    nftSuccessTimelock = _blocks;
    emit NumberUpdated("nftSuccessTimelock", _blocks);
  }

  function setSpreadReductionsP(uint256[5] calldata _r) external onlyGov {
    require(_r[0] > 0 && _r[1] > _r[0] && _r[2] > _r[1] && _r[3] > _r[2] && _r[4] > _r[3]);
    spreadReductionsP = _r;
    emit SpreadReductionsUpdated(_r);
  }

  function setMaxOpenInterestUsdc(uint256 _pairIndex, uint256 _newMaxOpenInterest) external onlyGov {
    // Can set max open interest to 0 to pause trading on this pair only
    openInterestUsdc[_pairIndex][2] = _newMaxOpenInterest;
    emit NumberUpdatedPair("maxOpenInterestUsdc", _pairIndex, _newMaxOpenInterest);
  }

  function setAgency(IHSAgency _hsAgency) external onlyGov {
    hsAgency = _hsAgency;
  }

  // Manage stored trades
  function storeTrade(Trade memory _trade, TradeInfo memory _tradeInfo) external onlyTrading {
    _trade.index = firstEmptyTradeIndex(_trade.trader, _trade.pairIndex);
    openTrades[_trade.trader][_trade.pairIndex][_trade.index] = _trade;

    openTradesCount[_trade.trader][_trade.pairIndex]++;
    tradesPerBlock[block.number]++;

    if (openTradesCount[_trade.trader][_trade.pairIndex] == 1) {
      pairTradersId[_trade.trader][_trade.pairIndex] = pairTraders[_trade.pairIndex].length;
      pairTraders[_trade.pairIndex].push(_trade.trader);
    }

    _tradeInfo.beingMarketClosed = false;
    openTradesInfo[_trade.trader][_trade.pairIndex][_trade.index] = _tradeInfo;

    updateOpenInterestUsdc(_trade.pairIndex, _tradeInfo.openInterestUsdc, true, _trade.buy);
  }

  function unregisterTrade(address trader, uint256 pairIndex, uint256 index) external onlyTrading {
    Trade storage t = openTrades[trader][pairIndex][index];
    TradeInfo storage i = openTradesInfo[trader][pairIndex][index];
    if (t.leverage == 0) {
      return;
    }

    updateOpenInterestUsdc(pairIndex, i.openInterestUsdc, false, t.buy);

    if (openTradesCount[trader][pairIndex] == 1) {
      uint256 _pairTradersId = pairTradersId[trader][pairIndex];
      address[] storage p = pairTraders[pairIndex];

      p[_pairTradersId] = p[p.length - 1];
      pairTradersId[p[_pairTradersId]][pairIndex] = _pairTradersId;

      delete pairTradersId[trader][pairIndex];
      p.pop();
    }

    delete openTrades[trader][pairIndex][index];
    delete openTradesInfo[trader][pairIndex][index];

    openTradesCount[trader][pairIndex]--;
    tradesPerBlock[block.number]++;
  }

  // Manage pending market orders
  function storePendingMarketOrder(PendingMarketOrder memory _order, uint256 _id, bool _open) external onlyTrading {
    pendingOrderIds[_order.trade.trader].push(_id);

    reqID_pendingMarketOrder[_id] = _order;
    reqID_pendingMarketOrder[_id].block = block.number;

    if (_open) {
      pendingMarketOpenCount[_order.trade.trader][_order.trade.pairIndex]++;
    } else {
      pendingMarketCloseCount[_order.trade.trader][_order.trade.pairIndex]++;
      openTradesInfo[_order.trade.trader][_order.trade.pairIndex][_order.trade.index].beingMarketClosed = true;
    }
    _addPendingOrderId(_id);
  }

  function unregisterPendingMarketOrder(uint256 _id, bool _open) external onlyTrading {
    _unregisterPendingMarketOrder(_id, _open);
  }

  // Manage open interest
  function updateOpenInterestUsdc(uint256 _pairIndex, uint256 _leveragedPosUsdc, bool _open, bool _long) private {
    uint256 index = _long ? 0 : 1;
    uint256[3] storage o = openInterestUsdc[_pairIndex];
    o[index] = _open ? o[index] + _leveragedPosUsdc : o[index] - _leveragedPosUsdc;
    emit OpenInterestExecuted(_pairIndex, _open, _long, o);
  }

  // Manage open limit orders
  function storeOpenLimitOrder(OpenLimitOrder memory o) external onlyTrading {
    o.index = firstEmptyOpenLimitIndex(o.trader, o.orderInfo.pairIndex);
    o.block = block.number;
    openLimitOrders.push(o);
    openLimitOrderIds[o.trader][o.orderInfo.pairIndex][o.index] = openLimitOrders.length - 1;
    openLimitOrdersCount[o.trader][o.orderInfo.pairIndex]++;
  }

  function updateOpenLimitOrder(OpenLimitOrder calldata _o) external onlyTrading {
    if (!hasOpenLimitOrder(_o.trader, _o.orderInfo.pairIndex, _o.index)) {
      return;
    }
    OpenLimitOrder storage o = openLimitOrders[openLimitOrderIds[_o.trader][_o.orderInfo.pairIndex][_o.index]];
    o.orderInfo.positionSize = _o.orderInfo.positionSize;
    o.orderInfo.buy = _o.orderInfo.buy;
    o.orderInfo.leverage = _o.orderInfo.leverage;
    o.orderInfo.tp = _o.orderInfo.tp;
    o.orderInfo.sl = _o.orderInfo.sl;
    o.orderInfo.minPrice = _o.orderInfo.minPrice;
    o.orderInfo.maxPrice = _o.orderInfo.maxPrice;
    o.block = block.number;
  }

  function unregisterOpenLimitOrder(address _trader, uint256 _pairIndex, uint256 _index) external onlyTrading {
    if (!hasOpenLimitOrder(_trader, _pairIndex, _index)) {
      return;
    }

    // Copy last order to deleted order => update id of this limit order
    uint256 id = openLimitOrderIds[_trader][_pairIndex][_index];
    openLimitOrders[id] = openLimitOrders[openLimitOrders.length - 1];
    openLimitOrderIds[openLimitOrders[id].trader][openLimitOrders[id].orderInfo.pairIndex][
      openLimitOrders[id].index
    ] = id;

    // Remove
    delete openLimitOrderIds[_trader][_pairIndex][_index];
    openLimitOrders.pop();

    openLimitOrdersCount[_trader][_pairIndex]--;
  }

  // Manage NFT orders
  function storePendingNftOrder(PendingNftOrder memory _nftOrder, uint256 _orderId) external onlyTrading {
    reqID_pendingNftOrder[_orderId] = _nftOrder;
  }

  function unregisterPendingNftOrder(uint256 _order) external onlyTrading {
    delete reqID_pendingNftOrder[_order];
  }

  // Manage open trade
  function updateSl(address _trader, uint256 _pairIndex, uint256 _index, uint256 _newSl) external onlyTrading {
    Trade storage t = openTrades[_trader][_pairIndex][_index];
    TradeInfo storage i = openTradesInfo[_trader][_pairIndex][_index];
    if (t.leverage == 0) {
      return;
    }
    t.sl = _newSl;
    i.slLastUpdated = block.number;
  }

  function updateTp(address _trader, uint256 _pairIndex, uint256 _index, uint256 _newTp) external onlyTrading {
    Trade storage t = openTrades[_trader][_pairIndex][_index];
    TradeInfo storage i = openTradesInfo[_trader][_pairIndex][_index];
    if (t.leverage == 0) {
      return;
    }
    t.tp = _newTp;
    i.tpLastUpdated = block.number;
  }

  function updateTrade(Trade memory _t) external onlyTrading {
    // useful when partial adding/closing
    Trade storage t = openTrades[_t.trader][_t.pairIndex][_t.index];
    if (t.leverage == 0) {
      return;
    }
    t.initialPosToken = _t.initialPosToken;
    t.positionSizeUsdc = _t.positionSizeUsdc;
    t.openPrice = _t.openPrice;
    t.leverage = _t.leverage;
  }

  // Manage rewards
  function distributeLpRewards(uint256 _amount) external onlyTrading {}

  function increaseNftRewards(uint256 _nftId, uint256 _amount) external onlyTrading {
    nftLastSuccess[_nftId] = block.number;
    nftRewards += _amount;
  }

  // Unlock next leverage
  function setLeverageUnlocked(address _trader, uint256 _newLeverage) external onlyTrading {
    traders[_trader].leverageUnlocked = _newLeverage;
  }

  // Manage gold & gov fees
  // Handle agencyfee
  function handleGoldGovFees(
    uint256 _pairIndex,
    uint256 _leveragedPositionSize,
    uint256,
    address,
    bool _fullFee
  ) external onlyTrading returns (uint256 fee) {
    fee = (_leveragedPositionSize * priceAggregator.openFeeP(_pairIndex)) / PRECISION / 100;
    if (!_fullFee) {
      fee /= 2;
    }
    fee *= 2;
    govFeesUsdc += fee;
  }

  function chargeGovFees(uint256 _govFee, bool) external onlyTrading {
    govFeesUsdc += _govFee;
  }

  function claimFees() external onlyGov {
    usdc.transfer(gov, govFeesUsdc);
    usdc.transfer(goldManager, goldFeesUsdc);
    goldFeesToken = govFeesToken = goldFeesUsdc = govFeesUsdc = 0;
  }

  // Manage tokens
  function handleTokens(address _a, uint256 _amount, bool _mint) external onlyTrading {
    if (_mint) {
      token.mint(_a, _amount);
      tokensMinted += _amount;
    } else {
      token.burn(_a, _amount);
      tokensBurned += _amount;
    }
  }

  function transferUsdc(address _from, address _to, uint256 _amount) external onlyTrading {
    if (_from == address(this)) {
      usdc.transfer(_to, _amount);
    } else {
      usdc.transferFrom(_from, _to, _amount);
    }
  }

  function transferLinkToAggregator(
    address _from,
    uint256 _pairIndex,
    uint256 _leveragedPosUsdc
  ) external onlyTrading {}

  // View utils functions
  function firstEmptyTradeIndex(address trader, uint256 pairIndex) public view returns (uint256 index) {
    for (uint256 i = 0; i < maxTradesPerPair; i++) {
      if (openTrades[trader][pairIndex][i].leverage == 0) {
        index = i;
        break;
      }
    }
  }

  function firstEmptyOpenLimitIndex(address trader, uint256 pairIndex) public view returns (uint256 index) {
    for (uint256 i = 0; i < maxTradesPerPair; i++) {
      if (!hasOpenLimitOrder(trader, pairIndex, i)) {
        index = i;
        break;
      }
    }
  }

  function hasOpenLimitOrder(address trader, uint256 pairIndex, uint256 index) public view returns (bool) {
    if (openLimitOrders.length == 0) {
      return false;
    }
    OpenLimitOrder storage o = openLimitOrders[openLimitOrderIds[trader][pairIndex][index]];
    return o.trader == trader && o.orderInfo.pairIndex == pairIndex && o.index == index;
  }

  // Additional getters
  function getReferral(address _trader) external view returns (address) {
    return traders[_trader].referral;
  }

  function getLeverageUnlocked(address _trader) external view returns (uint256) {
    return traders[_trader].leverageUnlocked;
  }

  function pairTradersArray(uint256 _pairIndex) external view returns (address[] memory) {
    return pairTraders[_pairIndex];
  }

  function getPendingOrderIds(address _trader) external view returns (uint256[] memory) {
    return pendingOrderIds[_trader];
  }

  function pendingOrderIdsCount(address _trader) external view returns (uint256) {
    return pendingOrderIds[_trader].length;
  }

  function getOpenLimitOrder(
    address _trader,
    uint256 _pairIndex,
    uint256 _index
  ) external view returns (OpenLimitOrder memory) {
    require(hasOpenLimitOrder(_trader, _pairIndex, _index));
    return openLimitOrders[openLimitOrderIds[_trader][_pairIndex][_index]];
  }

  function getOpenLimitOrders() external view returns (OpenLimitOrder[] memory) {
    return openLimitOrders;
  }

  function getSpreadReductionsArray() external view returns (uint256[5] memory) {
    return spreadReductionsP;
  }

  function removePendingCloseOrder(uint256 _orderId) external {
    PendingMarketOrder memory o = reqID_pendingMarketOrder[_orderId];
    //check is closeOrder
    if (o.trade.trader != address(0) && o.trade.positionSizeUsdc == 0) {
      //check openTrades
      Trade memory t = openTrades[o.trade.trader][o.trade.pairIndex][o.trade.index];
      //already closed
      if (t.positionSizeUsdc == 0) {
        _unregisterPendingMarketOrder(_orderId, false);
      }
    }
  }

  function getPendingOrderIds() external view returns (uint256[] memory) {
    return currentPendingOrderIds;
  }

  function _unregisterPendingMarketOrder(uint256 _id, bool _open) private {
    PendingMarketOrder memory _order = reqID_pendingMarketOrder[_id];
    uint256[] storage orderIds = pendingOrderIds[_order.trade.trader];

    for (uint256 i = 0; i < orderIds.length; i++) {
      if (orderIds[i] == _id) {
        if (_open) {
          pendingMarketOpenCount[_order.trade.trader][_order.trade.pairIndex]--;
        } else {
          pendingMarketCloseCount[_order.trade.trader][_order.trade.pairIndex]--;
          openTradesInfo[_order.trade.trader][_order.trade.pairIndex][_order.trade.index].beingMarketClosed = false;
        }

        orderIds[i] = orderIds[orderIds.length - 1];
        orderIds.pop();

        delete reqID_pendingMarketOrder[_id];
        _removePendingOrderId(_id);
        return;
      }
    }
  }

  function _addPendingOrderId(uint256 _pendingOrderId) private {
    currentPendingOrderIds.push(_pendingOrderId);
    indexOfPendingOrderIds[_pendingOrderId] = currentPendingOrderIds.length - 1;
  }

  function _removePendingOrderId(uint _pendingOrderId) private {
    uint index = indexOfPendingOrderIds[_pendingOrderId];
    if (currentPendingOrderIds.length == 0 || (index == 0 && currentPendingOrderIds[0] != _pendingOrderId)) {
      return;
    }

    if (currentPendingOrderIds.length > 1) {
      currentPendingOrderIds[index] = currentPendingOrderIds[currentPendingOrderIds.length - 1];
      indexOfPendingOrderIds[currentPendingOrderIds[index]] = index;
    }
    delete indexOfPendingOrderIds[_pendingOrderId];
    currentPendingOrderIds.pop(); // Implicitly recovers gas from last element storage
  }
}
