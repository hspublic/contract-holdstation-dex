// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

import {ERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {IERC20Extend} from "../interfaces/IERC20Extend.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "../helpers/TransferHelper.sol";

contract ConvertHSTCredit is Ownable, ReentrancyGuard {
  using SafeMath for uint256;
  uint256 constant PRECISION = 1e10;

  uint256 public rate; //rate hst <-> hold
  uint256 public minAmount;
  IERC20Extend public hsTokenCredit;
  address public holdToken;

  event SetRate(uint256 rate);
  event SetMinAmount(uint256 minAmount);
  event Converted(address indexed sender, uint256 hstAmount, uint256 holdAmount);

  constructor(address _hsTokenCredit, address _holdToken) {
    hsTokenCredit = IERC20Extend(_hsTokenCredit);
    holdToken = _holdToken;
    minAmount = 10 * 1e18; //min = 10HST
    rate = 1e8; //1%
  }

  modifier isValidAmount(uint _amount) {
    require(_amount >= minAmount, "WRONG_AMOUNT");
    _;
  }

  function setRate(uint _rate) external onlyOwner {
    rate = _rate;
    emit SetRate(_rate);
  }

  function setMinAmount(uint _minAmount) external onlyOwner {
    minAmount = _minAmount;
    emit SetMinAmount(_minAmount);
  }

  function convert(uint _amount) external isValidAmount(_amount) nonReentrant {
    address sender = _msgSender();
    require(_amount <= hsTokenCredit.balanceOf(sender), "INSUFFICIENT_BALANCE");
    uint holdAmount = _amount.mul(rate).div(PRECISION);
    hsTokenCredit.burn(sender, _amount);
    TransferHelper.safeTransfer(holdToken, sender, holdAmount);
    emit Converted(sender, _amount, holdAmount);
  }

  function claimStuckTokens(address _token) external onlyOwner {
    require(_token != address(this), "INVALID_HOLDER");
    if (_token == address(0)) {
      (bool suceess, ) = msg.sender.call{value: address(this).balance}("");
      if (!suceess) revert("WITHDRAWAL_FAILED");
      return;
    }
    IERC20Extend(_token).transfer(msg.sender, IERC20Extend(_token).balanceOf(address(this)));
  }
}
