// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/MathUpgradeable.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../helpers/TransferHelper.sol";
import {IERC20Extend} from "../interfaces/IERC20Extend.sol";

contract HSVestingESHold is Initializable, OwnableUpgradeable, ReentrancyGuardUpgradeable {
  using MathUpgradeable for uint256;
  uint256 public constant PERCENTAGE_BASE = 1000;

  enum StakingType {
    FLEXIBLE,
    LOCK_MEDIUM_EPOCHS,
    LOCK_HIGH_EPOCHS
  }

  struct RewardDetail {
    uint epoch;
    uint rewardAmount;
  }
  uint public currentEpoch;
  uint public nextEpochTime;
  uint public requestEvery;

  IERC20Extend public esHold;
  address public holdToken;
  mapping(StakingType => uint) public rewardP;
  mapping(StakingType => uint) public lockedEpochs;

  mapping(address => uint) public claimedAtEpoch;
  //user->claimepoch -> amount
  mapping(address => mapping(uint256 => uint256)) public stakingInfos;
  event Staked(address indexed sender, uint256 amount, uint earningAmount);
  event ClaimedAtEpoch(address indexed sender, uint amount, uint atEpoch);
  event Claimed(address indexed sender, uint amount, uint atEpoch);
  event SetRewardP(StakingType stakingType, uint256 rewardP);
  event SetLockEpoch(StakingType stakingType, uint256 totalEpoch);
  event EpochForced(uint currentEpoch, uint currentTime, uint nextTime);
  event RequestEveryConfigured(uint requestEvery);

  function initialize(IERC20Extend _esHold, address _holdToken) external initializer {
    __Ownable_init();
    __ReentrancyGuard_init();
    esHold = _esHold;
    holdToken = _holdToken;
    requestEvery = 4 days;
    nextEpochTime = block.timestamp + requestEvery;
    rewardP[StakingType.FLEXIBLE] = 500;
    rewardP[StakingType.LOCK_MEDIUM_EPOCHS] = 750;
    rewardP[StakingType.LOCK_HIGH_EPOCHS] = 1000;
    lockedEpochs[StakingType.FLEXIBLE] = 0;
    lockedEpochs[StakingType.LOCK_MEDIUM_EPOCHS] = 3;
    lockedEpochs[StakingType.LOCK_HIGH_EPOCHS] = 5;
  }

  function setRewardP(StakingType _type, uint _rewardP) external onlyOwner {
    require(_rewardP <= PERCENTAGE_BASE, "INVALID_PERCENTAGE");
    rewardP[_type] = _rewardP;
    emit SetRewardP(_type, _rewardP);
  }

  function setLockEpoch(StakingType _type, uint _totalEpoch) external onlyOwner {
    lockedEpochs[_type] = _totalEpoch;
    emit SetLockEpoch(_type, _totalEpoch);
  }

  function setRequestEvery(uint _requestEvery) external onlyOwner {
    requestEvery = _requestEvery;
    emit RequestEveryConfigured(_requestEvery);
  }

  function forceNewEpoch() external {
    require(nextEpochTime != 0 && block.timestamp >= nextEpochTime, "TIME_NOT_REACH");
    currentEpoch++;
    nextEpochTime = block.timestamp + requestEvery;
    emit EpochForced(currentEpoch, block.timestamp, nextEpochTime);
  }

  function stake(StakingType _stakingType, uint _amount) external nonReentrant {
    uint rewardPercent = rewardP[_stakingType];
    require(rewardPercent > 0, "ZERO_PERCENTAGE");
    address sender = _msgSender();
    require(esHold.balanceOf(sender) >= _amount, "INSUFFICIENT_BALANCE");

    uint epochCanClaim = currentEpoch + lockedEpochs[_stakingType];

    esHold.burn(sender, _amount);
    uint earningAmount = _amount.mulDiv(rewardPercent, PERCENTAGE_BASE, MathUpgradeable.Rounding.Down);
    stakingInfos[sender][epochCanClaim] += earningAmount;
    emit Staked(sender, _amount, earningAmount);
  }

  function claim() external nonReentrant {
    address sender = _msgSender();
    uint earning = 0;
    for (uint i = claimedAtEpoch[sender]; i <= currentEpoch; i++) {
      earning += stakingInfos[sender][i];
      emit ClaimedAtEpoch(sender, stakingInfos[sender][i], i);
      delete stakingInfos[sender][i];
    }
    require(earning > 0, "NOTHING_TO_CLAIM");
    claimedAtEpoch[sender] = currentEpoch;
    TransferHelper.safeTransfer(holdToken, sender, earning);
    emit Claimed(sender, earning, currentEpoch);
  }

  function calculateReward(address _sender) external view returns (uint) {
    uint earning = 0;
    for (uint i = claimedAtEpoch[_sender]; i <= currentEpoch; i++) {
      earning += stakingInfos[_sender][i];
    }
    return earning;
  }

  function estimateReward(address _sender) external view returns (RewardDetail[] memory) {
    uint to = lockedEpochs[StakingType.LOCK_HIGH_EPOCHS];
    RewardDetail[] memory rewardDetails = new RewardDetail[](to);
    for (uint i = 0; i < to; ++i) {
      rewardDetails[i] = RewardDetail({
        epoch: currentEpoch + i + 1,
        rewardAmount: stakingInfos[_sender][currentEpoch + i + 1]
      });
    }
    return rewardDetails;
  }
}
