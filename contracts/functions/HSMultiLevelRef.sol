// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../interfaces/IHSMultiLevelRef.sol";

contract HSMultiLevelRef is Initializable, AccessControlUpgradeable, IHSMultiLevelRef {
  bytes32 public constant MANAGER_ROLE = keccak256("MANAGER_ROLE");
  mapping(address => address) public kolReferrer;
  mapping(address => DirectReferrer) public directReferrer;

  event KOLRegistered(address indexed kol, address manager);
  event ReferrerRegistered(address indexed referree, address referrer, address kol, Level level);

  function initialize() external initializer {
    _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
    _setupRole(MANAGER_ROLE, msg.sender);
  }

  modifier only(bytes32 role) {
    require(hasRole(role, _msgSender()), "INSUFFICIENT_PERMISSIONS");
    _;
  }

  function addKOL(address _kol) external only(MANAGER_ROLE) {
    require(kolReferrer[_kol] != _kol, "EXISTED");
    require(directReferrer[_kol].referrer == address(0), "ON_CHAIN");
    kolReferrer[_kol] = _kol;
    emit KOLRegistered(_kol, msg.sender);
  }

  function registerReferrer(address _addressReferrer) external {
    require(kolReferrer[_addressReferrer] != address(0), "KOL_NOT_ACTIVE");
    require(directReferrer[msg.sender].referrer == address(0), "ON_CHAIN");
    require(
      kolReferrer[_addressReferrer] == _addressReferrer ||
        (directReferrer[_addressReferrer].referrer != address(0) &&
          uint(directReferrer[_addressReferrer].level) < uint(Level.LEVEL2)),
      "INVALID_REFERRER"
    );

    Level refLevel = Level.KOL;
    //ref other level
    if (kolReferrer[_addressReferrer] != _addressReferrer) {
      refLevel = Level(uint(directReferrer[_addressReferrer].level) + 1);
    }
    DirectReferrer memory referrer = DirectReferrer(_addressReferrer, refLevel);

    directReferrer[msg.sender] = referrer;
    kolReferrer[msg.sender] = kolReferrer[_addressReferrer];
    emit ReferrerRegistered(msg.sender, _addressReferrer, kolReferrer[_addressReferrer], refLevel);
  }

  function getDirectReferrer(address referrer) public view override returns (DirectReferrer memory) {
    return directReferrer[referrer];
  }
}
