// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import {IERC20Extend} from "../interfaces/IERC20Extend.sol";
import "../interfaces/NftInterfaceV5.sol";
import "../interfaces/TokenInterfaceV5.sol";

contract HSStaking is Initializable, OwnableUpgradeable, ReentrancyGuardUpgradeable {
  uint256 public constant PERCENTAGE_BASE = 1e10;
  // Contracts & Addresses
  address public admin;
  TokenInterfaceV5 public token; // Hold token
  TokenInterfaceV5 public usdc; //usdc
  TokenInterfaceV5 public rewardToken; //esHold
  uint256 public rewardP;
  // Pool state
  uint256 public accUsdcPerToken;
  uint256 public tokenBalance;
  // Pool stats
  uint256 public totalRewardsDistributedUsdc; // 1e18

  uint256 public timelock;
  uint256 public currentEpochStartAt; //time epoch was forced
  uint256 public currentEpoch;
  uint256 public unstakeNumberEpochs;
  // Mappings
  mapping(address => User) public users;
  //unstake request
  mapping(address => mapping(uint256 => uint256)) public unstakeRequests;
  mapping(address => uint256) public unstakeVolume;

  mapping(address => mapping(uint256 => UserReward)) public esHoldRewards; //user => epoch => reward
  mapping(address => uint256) public claimedAtEpoch;
  mapping(uint => uint256) public epochRewards;
  struct User {
    uint256 stakedTokens; // 1e18
    uint256 debtUsdc; // 1e18
    uint256 harvestedRewardsUsdc; // 1e18
  }

  struct UserReward {
    uint256 total;
    bool isEmpty;
  }

  // Events
  event AdminUpdated(address value);
  event RewardUpdated(uint value);
  event TimelockUpdated(uint value);
  event UnstakeNumberEpochs(uint value);
  event NewEpochForced(uint currentEpoch, uint startAt);
  event UsdcDistributed(uint256 amount);
  event UsdcHarvested(address indexed user, uint256 amount);
  event TokensStaked(address indexed user, uint256 amount, uint epoch, uint esHoldReward);
  event TokensUnstaked(address indexed user, uint256 amount, uint unstakeEpoch, uint currentEpoch, uint esHoldReward);
  event RequestCreated(address indexed user, uint amount, uint epochCanClaim, uint totalRequestVolume);
  event ESHoldClaimed(address indexed user, uint reward, uint currentEpoch, uint nextEpochRewarding);

  function initialize(
    TokenInterfaceV5 _token,
    TokenInterfaceV5 _usdc,
    TokenInterfaceV5 _rewardToken
  ) external initializer {
    __Ownable_init();
    __ReentrancyGuard_init();
    require(
      address(_token) != address(0) && address(_usdc) != address(0) && address(_rewardToken) != address(0),
      "WRONG_PARAMS"
    );
    admin = msg.sender;
    token = _token;
    usdc = _usdc;
    rewardToken = _rewardToken;
    rewardP = 10 * 1e8;
    currentEpochStartAt = block.timestamp;
    timelock = 3 days;
    unstakeNumberEpochs = 5;
    epochRewards[currentEpoch] = rewardP;
  }

  // Modifiers
  modifier onlyAdmin() {
    require(_msgSender() == admin, "GOV_ONLY");
    _;
  }

  // Manage addresses
  function setAdmin(address _value) external onlyOwner {
    require(_value != address(0), "ADDRESS_0");
    admin = _value;
    emit AdminUpdated(_value);
  }

  function setRewardP(uint _rewardP) external onlyAdmin {
    require(_rewardP <= PERCENTAGE_BASE, "INVALID_DATA");
    rewardP = _rewardP;
    emit RewardUpdated(_rewardP);
  }

  function setTimelock(uint _timelock) external onlyAdmin {
    timelock = _timelock;
    emit TimelockUpdated(_timelock);
  }

  function setUnstakeNumberEpochs(uint _value) external onlyAdmin {
    unstakeNumberEpochs = _value;
    emit UnstakeNumberEpochs(_value);
  }

  function forceNewEpoch() external {
    require(block.timestamp - currentEpochStartAt >= timelock, "TOO_EARLY");
    currentEpoch++;
    epochRewards[currentEpoch] = rewardP;
    currentEpochStartAt = block.timestamp;
    emit NewEpochForced(currentEpoch, currentEpochStartAt);
  }

  // Distribute rewards
  function distributeRewardUsdc(uint256 _amount) external {
    usdc.transferFrom(_msgSender(), address(this), _amount);
    if (tokenBalance > 0) {
      accUsdcPerToken += (_amount * 1e18) / tokenBalance;
      totalRewardsDistributedUsdc += _amount;
    }
    emit UsdcDistributed(_amount);
  }

  function recalculateDebt() private {
    User storage u = users[msg.sender];
    u.debtUsdc = (u.stakedTokens * accUsdcPerToken) / 1e18;
  }

  // Rewards to be harvested
  function pendingRewardUsdc(address _sender) public view returns (uint256) {
    User storage u = users[_sender];
    return (u.stakedTokens * accUsdcPerToken) / 1e18 - u.debtUsdc;
  }

  // Harvest rewards
  function harvest() public {
    address staker = _msgSender();
    uint256 pendingUsdc = pendingRewardUsdc(staker);
    User storage u = users[staker];
    u.debtUsdc = (u.stakedTokens * accUsdcPerToken) / 1e18;
    u.harvestedRewardsUsdc += pendingUsdc;

    usdc.transfer(staker, pendingUsdc);

    emit UsdcHarvested(staker, pendingUsdc);
  }

  // Stake tokens
  function stake(uint256 _amount) external nonReentrant {
    require(_amount > 0, "ZERO_AMOUNT");
    address staker = _msgSender();
    User storage u = users[staker];
    token.transferFrom(staker, address(this), _amount);

    harvest();
    tokenBalance -= u.stakedTokens;
    u.stakedTokens += _amount;
    recalculateDebt();
    tokenBalance += u.stakedTokens;

    UserReward storage userReward = esHoldRewards[staker][currentEpoch + 1];
    userReward.total = u.stakedTokens;
    userReward.isEmpty = false;
    emit TokensStaked(staker, _amount, currentEpoch, userReward.total);
  }

  // Unstake tokens
  function unstake(uint256 _epoch) external nonReentrant {
    require(_epoch <= currentEpoch, "TIME_NOT_YET");
    address staker = _msgSender();
    uint amount = unstakeRequests[staker][_epoch];
    require(amount > 0, "INVALID_REQUEST");
    User storage u = users[staker];
    harvest();

    tokenBalance -= u.stakedTokens;
    u.stakedTokens -= amount;
    recalculateDebt();
    tokenBalance += u.stakedTokens;
    token.transfer(staker, amount);

    unstakeVolume[staker] -= unstakeRequests[staker][_epoch];
    delete unstakeRequests[staker][_epoch];

    UserReward storage userReward = esHoldRewards[staker][currentEpoch + 1];
    userReward.total = u.stakedTokens;
    userReward.isEmpty = userReward.total > 0 ? false : true;
    emit TokensUnstaked(staker, amount, _epoch, currentEpoch, userReward.total);
  }

  function makeRequest(uint256 _amount) external nonReentrant {
    require(_amount > 0, "INVALID_AMOUNT");
    address staker = _msgSender();
    User memory u = users[staker];
    require(u.stakedTokens >= unstakeVolume[staker] + _amount, "EXCEED_AMOUNT");
    unstakeRequests[staker][currentEpoch + unstakeNumberEpochs] += _amount;
    unstakeVolume[staker] += _amount;
    emit RequestCreated(staker, _amount, currentEpoch + unstakeNumberEpochs, unstakeVolume[staker]);
  }

  function getUnstakeRequests(
    uint _page,
    uint _limit,
    address _staker
  ) external view returns (uint[] memory, uint[] memory) {
    uint from = _page * _limit;
    uint to = _page * _limit + _limit;
    uint[] memory epochs = new uint[](_limit);
    uint[] memory pendings = new uint[](_limit);
    if (from > currentEpoch + unstakeNumberEpochs + 1 || to > currentEpoch + unstakeNumberEpochs + 1) {
      return (epochs, pendings);
    }
    uint count = 0;
    for (uint i = from; i < to; i++) {
      epochs[count] = i;
      pendings[count] = unstakeRequests[_staker][i];
      count++;
    }
    return (epochs, pendings);
  }

  function claimESHoldReward() external nonReentrant {
    address staker = _msgSender();
    uint reward = calculateEsHoldReward(staker);
    require(reward > 0, "ZERO_REWARD");
    rewardToken.mint(staker, reward);
    User memory u = users[staker];
    claimedAtEpoch[staker] = currentEpoch;
    esHoldRewards[staker][currentEpoch].total = 0;
    UserReward storage userReward = esHoldRewards[staker][currentEpoch + 1];
    userReward.total = u.stakedTokens;
    userReward.isEmpty = userReward.total > 0 ? false : true;
    emit ESHoldClaimed(staker, reward, currentEpoch, userReward.total);
  }

  function calculateEsHoldReward(address _staker) public view returns (uint) {
    uint totalReward = 0;
    uint lastChange = 0;
    for (uint i = claimedAtEpoch[_staker]; i <= currentEpoch; i++) {
      UserReward memory userReward = esHoldRewards[_staker][i];
      if (userReward.isEmpty) {
        lastChange = 0;
        continue;
      }
      if (userReward.total > 0) {
        lastChange = userReward.total;
      }
      totalReward += ((lastChange * epochRewards[i]) / PERCENTAGE_BASE);
    }
    return totalReward;
  }
}
