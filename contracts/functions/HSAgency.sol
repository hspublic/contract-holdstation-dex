// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../interfaces/IHSAgency.sol";
import "../interfaces/HSReferralsInterfaceV6_2.sol";
import "../interfaces/StorageInterfaceV5.sol";

contract HSAgency is Initializable, AccessControlUpgradeable, IHSAgency {
  HSReferralsInterfaceV6_2 public referralContract;
  StorageInterfaceV5 public storageT;
  bytes32 public constant MANAGER_ROLE = keccak256("MANAGER_ROLE");

  uint256 public constant PRECISION = 1e10;
  uint256[2] public distributionP;
  uint256 public discountFeeP;
  uint256 public tokensClaimedTotal; // 1e6
  uint256 public totalCurrentReward;

  mapping(address => address) public rootReferrer;
  mapping(address => DirectReferrer) public directReferrer;
  mapping(address => bool) public rootStatus;
  mapping(address => uint256) public reward;
  mapping(address => uint256) public tokensClaimed; // 1e6

  event RootRegistered(address indexed root, address manager);
  event ReferrerRegistered(address indexed referee, address referrer, address root, Level level);
  event UpdateRootStatus(address indexed root, address manager, bool status);
  event TokensClaimed(address user, uint256 amount);
  event RewardDistribution(address indexed user, uint256 fee, address referer, Level level);

  function initialize(HSReferralsInterfaceV6_2 _referralContract, StorageInterfaceV5 _storageT) external initializer {
    _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
    _setupRole(MANAGER_ROLE, msg.sender);
    storageT = _storageT;
    referralContract = _referralContract;
    distributionP = [1538461538, 4615384615]; //~15.38% for Lv0, ~46.15% for direct ref (1e10)
    discountFeeP = 80000000; //(1e10)
  }

  modifier only(bytes32 role) {
    require(hasRole(role, _msgSender()), "INSUFFICIENT_PERMISSIONS");
    _;
  }

  function updateDiscountFeeP(uint256 _discountFeeP) external only(DEFAULT_ADMIN_ROLE) {
    discountFeeP = _discountFeeP;
  }

  function updateDistributionP(uint256[2] memory _distributionP) external only(DEFAULT_ADMIN_ROLE) {
    distributionP = _distributionP;
  }

  function updateReferralContract(HSReferralsInterfaceV6_2 _referralContract) external only(DEFAULT_ADMIN_ROLE) {
    referralContract = _referralContract;
  }

  function setStorageT(StorageInterfaceV5 _storageT) external only(DEFAULT_ADMIN_ROLE) {
    storageT = _storageT;
  }

  function updateRootStatus(address _root, bool _status) external only(MANAGER_ROLE) {
    rootStatus[_root] = _status;
    emit UpdateRootStatus(_root, msg.sender, _status);
  }

  /**
   * @dev Add user as LEV0.
   *
   * Only manager who has permission interact with this function
   *
   * address's LEV0 != null address
   * The account must not exist in any ref network.
   */
  function addRoot(address _level0) external only(MANAGER_ROLE) {
    require(_level0 != address(0), "ADDRESS_0");
    require(address(referralContract) == address(0) || !referralContract.checkReferrerActive(_level0), "IS_REFERRER");
    require(rootReferrer[_level0] != _level0, "EXISTED");
    require(directReferrer[_level0].referrer == address(0), "ON_NETWORK");
    rootReferrer[_level0] = _level0;
    rootStatus[_level0] = true;
    emit RootRegistered(_level0, msg.sender);
  }

  /**
   * @dev Follow an user.
   *
   * Anyone could interact with this function
   *
   * require1+2: address Referrer must follow a LEV0 and LEV0 is activing
   * require3: sender must not exist in any ref network
   * require4: _addressReferrer is a LEV0 or (followed an address and maximum is level 2)
   * The account must not exist in any ref network.
   */

  function registerReferrer(address _addressReferrer) external {
    require(msg.sender != _addressReferrer, "SAME_SENDER");
    require(
      address(referralContract) == address(0) || !referralContract.checkReferrerActive(_addressReferrer),
      "IS_REFERRER"
    );
    require(rootReferrer[_addressReferrer] != address(0), "OUT_OF_NETWORK");
    require(rootStatus[rootReferrer[_addressReferrer]], "LEV0_WERE_DISABLED");
    require(rootReferrer[msg.sender] == address(0), "ON_NETWORK");
    require(
      rootReferrer[_addressReferrer] == _addressReferrer ||
        (directReferrer[_addressReferrer].referrer != address(0) &&
          uint(directReferrer[_addressReferrer].level) < uint(Level.LEVEL2)),
      "INVALID_REFERRER"
    );

    Level refLevel = Level.LEVEL0;
    //ref other level
    if (rootReferrer[_addressReferrer] != _addressReferrer) {
      refLevel = Level(uint(directReferrer[_addressReferrer].level) + 1);
    }
    DirectReferrer memory referrer = DirectReferrer(_addressReferrer, refLevel);

    directReferrer[msg.sender] = referrer;
    rootReferrer[msg.sender] = rootReferrer[_addressReferrer];
    //refLevel is referrer level
    emit ReferrerRegistered(msg.sender, _addressReferrer, rootReferrer[_addressReferrer], refLevel);
  }

  function getDirectReferrer(address referrer) public view override returns (DirectReferrer memory) {
    return directReferrer[referrer];
  }

  function getDistributionP(address _user) public view override returns (uint256 feeP1, uint256 feeP2) {
    if (
      _user == address(0) ||
      rootReferrer[_user] == address(0) ||
      rootReferrer[_user] == _user ||
      !rootStatus[rootReferrer[_user]]
    ) {
      return (uint256(0), uint256(0));
    }
    //lv1
    if (directReferrer[_user].level == Level.LEVEL0) {
      return (distributionP[0] + distributionP[1], uint256(0));
    }
    if (directReferrer[_user].level == Level.LEVEL1 || directReferrer[_user].level == Level.LEVEL2) {
      return (distributionP[0], distributionP[1]);
    }
    return (uint256(0), uint256(0));
  }

  function calulateFee(uint256 _vaultOpenFeeP, address _user) public view override returns (uint256) {
    if (rootReferrer[_user] != address(0) && rootStatus[rootReferrer[_user]]) {
      return _vaultOpenFeeP > discountFeeP ? _vaultOpenFeeP - discountFeeP : _vaultOpenFeeP;
    }
    return _vaultOpenFeeP;
  }

  function distributeReward(uint256 _fullFee, address _user) external override returns (uint256) {
    require(msg.sender == address(storageT), "ONLY_TRADING_STORAGE");

    //feeP1 for root, feeP2 for direct referrer
    uint256 referralFee = 0;
    (uint256 feeP1, uint256 feeP2) = getDistributionP(_user);
    if (feeP1 > 0) {
      uint256 referralFee1 = (_fullFee * feeP1) / PRECISION;
      referralFee += referralFee1;
      reward[rootReferrer[_user]] += referralFee1;
      emit RewardDistribution(_user, referralFee1, rootReferrer[_user], Level.LEVEL0);
    }

    if (feeP2 > 0) {
      uint256 referralFee2 = (_fullFee * feeP2) / PRECISION;
      referralFee += referralFee2;
      reward[directReferrer[_user].referrer] += referralFee2;
      emit RewardDistribution(_user, referralFee2, directReferrer[_user].referrer, directReferrer[_user].level);
    }
    totalCurrentReward += referralFee;
    return referralFee;
  }

  function claimRewards() external {
    uint256 tokens = reward[msg.sender];
    require(tokens > 0, "NOTHING_TO_CLAIM");
    require(totalCurrentReward >= tokens, "EXCEED_AVAILABLE");
    reward[msg.sender] = 0;
    totalCurrentReward -= tokens;
    storageT.transferUsdc(address(storageT), msg.sender, tokens);
    tokensClaimed[msg.sender] += tokens;
    tokensClaimedTotal += tokens;
    emit TokensClaimed(msg.sender, tokens);
  }
}
