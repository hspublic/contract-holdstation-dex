// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";

contract HSAirdrop is Initializable, OwnableUpgradeable, ReentrancyGuardUpgradeable {
  IERC20 public holdToken;
  uint public startTime;
  uint public endTime;
  bytes32 public merkleRoot;
  mapping(address => bool) public claimed;

  event Claimed(address indexed sender, uint256 amount);
  event TakenBack(address receiver, uint256 amount);

  function initialize(IERC20 _holdToken) external initializer {
    __Ownable_init();
    __ReentrancyGuard_init();
    holdToken = _holdToken;
  }

  modifier isActive() {
    require(block.timestamp >= startTime && block.timestamp <= endTime, "INACTIVE");
    _;
  }

  function setStartTime(uint _startTime) external onlyOwner {
    startTime = _startTime;
  }

  function setEndTime(uint _endTime) external onlyOwner {
    endTime = _endTime;
  }

  function deplete(address _receiver) external onlyOwner {
    takeBack(_receiver, holdToken.balanceOf(address(this)));
  }

  function takeBack(address _receiver, uint _amount) public onlyOwner {
    require(holdToken.balanceOf(address(this)) >= _amount, "EXCEED_BALANCE");
    holdToken.transfer(_receiver, _amount);
    emit TakenBack(_receiver, _amount);
  }

  function setRoot(bytes32 _merkleRoot) external onlyOwner {
    merkleRoot = _merkleRoot;
  }

  function claim(uint _amount, bytes32[] calldata _merkleProof) external isActive nonReentrant {
    address sender = _msgSender();
    require(_amount > 0, "ZERO_AMOUNT");
    require(!claimed[sender], "ALREADY_CLAIMED");
    //bytes32 node = keccak256(abi.encodePacked(sender, _amount));
    bytes32 node = keccak256(bytes.concat(keccak256(abi.encode(sender, _amount))));
    require(MerkleProof.verify(_merkleProof, merkleRoot, node), "INVALID_PROOF");
    claimed[sender] = true;
    holdToken.transfer(sender, _amount);
    emit Claimed(sender, _amount);
  }
}
