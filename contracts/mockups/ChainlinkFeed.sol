// SPDX-License-Identifier: MIT

pragma solidity 0.8.10;
import "@openzeppelin/contracts/access/Ownable.sol";
import "./AggregatorFeed.sol";

contract ChainlinkFeed is Ownable {
  IAggregatorFeed private _aggFeed;
  int256 public _fakeAnswer;

  constructor() {}

  function latestRoundData()
    public
    view
    returns (uint80 roundId, int256 answer, uint256 startedAt, uint256 updatedAt, uint80 answeredInRound)
  {
    if (address(_aggFeed) == address(0)) {
      return (0, _fakeAnswer, 0, 0, 0);
    }
    (roundId, answer, startedAt, updatedAt, answeredInRound) = _aggFeed.latestRoundData();
    return (roundId, answer, startedAt, updatedAt, answeredInRound);
  }

  function setAggregator(IAggregatorFeed aggFeed) external onlyOwner {
    _aggFeed = aggFeed;
  }

  function pairName() external view returns (string memory) {
    return _aggFeed.pairName();
  }

  function getAggregator() external view onlyOwner returns (address) {
    return address(_aggFeed);
  }

  function setAnswer(int256 answer) external onlyOwner {
    _fakeAnswer = answer;
  }
}
