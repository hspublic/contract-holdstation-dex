// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

/**
 * @title Ballot
 * @dev Implements voting process along with vote delegation
 */
contract TestChainData {
  function getBlockNumber() external view returns (uint256) {
    return block.number;
  }

  function getBlockTime() external view returns (uint256) {
    return block.timestamp;
  }
}
