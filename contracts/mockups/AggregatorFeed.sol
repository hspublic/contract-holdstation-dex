// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

interface IAggregatorFeed {
  function latestRoundData()
    external
    view
    returns (uint80 roundId, int256 answer, uint256 startedAt, uint256 updatedAt, uint80 answeredInRound);

  function getRoundData(
    uint256 _roundId
  ) external view returns (uint80 roundId, int256 answer, uint256 startedAt, uint256 updatedAt, uint80 answeredInRound);

  function pairName() external view returns (string memory);

  function getAnswer(uint256 _roundId) external view returns (int192);
}

contract AggregatorFeed is Ownable, IAggregatorFeed {
  using Counters for Counters.Counter;
  int256 public fakeAnswer = 100_000 * 1e10;
  Counters.Counter public latestAggregatorRoundId;
  mapping(address => bool) public verifiers;
  mapping(address => bool) public whitelistCallers;
  string public pairName;

  struct Transmission {
    int192 answer; // 192 bits ought to be enough for anyone
    uint256 timestamp;
  }

  //RoundId -> Transmission
  mapping(uint256 => Transmission) internal transmissions;

  constructor(string memory _pairName) {
    if (bytes(_pairName).length > 0) {
      pairName = _pairName;
    }
  }

  modifier onlyVerifier() {
    require(verifiers[msg.sender], "PERMISSION_DENIED");
    _;
  }

  modifier onlyWhitelist() {
    require(whitelistCallers[msg.sender], "NOT_WHITELIST");
    _;
  }

  function latestRoundData()
    public
    view
    override
    onlyWhitelist
    returns (uint80 roundId, int256 answer, uint256 startedAt, uint256 updatedAt, uint80 answeredInRound)
  {
    return (
      uint80(latestAggregatorRoundId.current()),
      transmissions[latestAggregatorRoundId.current()].answer,
      transmissions[latestAggregatorRoundId.current()].timestamp,
      transmissions[latestAggregatorRoundId.current()].timestamp,
      uint80(latestAggregatorRoundId.current())
    );
  }

  function getAnswer(uint256 _roundId) external view returns (int192) {
    return transmissions[_roundId].answer;
  }

  function getRoundData(
    uint256 _roundId
  )
    external
    view
    returns (uint80 roundId, int256 answer, uint256 startedAt, uint256 updatedAt, uint80 answeredInRound)
  {
    Transmission memory transmission = transmissions[_roundId];
    return (uint80(_roundId), transmission.answer, transmission.timestamp, transmission.timestamp, uint80(_roundId));
  }

  function updateAnswer(int192 _answer) external onlyVerifier {
    latestAggregatorRoundId.increment();
    transmissions[latestAggregatorRoundId.current()].answer = _answer;
    transmissions[latestAggregatorRoundId.current()].timestamp = block.timestamp;
  }

  function addVerifier(address verifier, bool status) external onlyOwner {
    verifiers[verifier] = status;
  }

  function addWhitelist(address whitelist, bool status) external onlyOwner {
    whitelistCallers[whitelist] = status;
  }
}
