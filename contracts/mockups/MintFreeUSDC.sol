// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

import "@openzeppelin/contracts/access/Ownable.sol";
import "../interfaces/TokenInterfaceV5.sol";

contract FreeMintUSDC is Ownable {
  TokenInterfaceV5 public usdcHS;
  uint256 public freeMint;

  constructor(TokenInterfaceV5 _usdcHS) {
    usdcHS = _usdcHS;
    freeMint = 10_000;
  }

  function setToken(TokenInterfaceV5 _usdcHS) external onlyOwner {
    usdcHS = _usdcHS;
  }

  function setFreeMint(uint256 _freeMint) external onlyOwner {
    freeMint = _freeMint;
  }

  // Get 1000 free USDC
  function getFreeUSDC() external {
    usdcHS.mint(msg.sender, freeMint * 1e6);
  }
}
