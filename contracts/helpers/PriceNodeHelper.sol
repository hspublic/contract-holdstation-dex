// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;
import "../interfaces/StorageInterfaceV5.sol";
import "../interfaces/AggregatorInterfaceV6.sol";

contract PriceNodeHelper {
  StorageInterfaceV5 public immutable storageT;
  NftRewardsInterfaceV6 public immutable nftRewards;
  enum LimitOrder {
    TP,
    SL,
    LIQ,
    OPEN_REVERSAL,
    OPEN_MOMENTUM
  }

  constructor(StorageInterfaceV5 _storageT, NftRewardsInterfaceV6 _nftRewards) {
    require(address(_storageT) != address(0), "WRONG_PARAMS");
    storageT = _storageT;
    nftRewards = _nftRewards;
  }

  function getOrderInfo(
    uint256 orderId,
    AggregatorInterfaceV6.OrderType orderType
  )
    external
    view
    returns (
      LimitOrder limitOrderType,
      StorageInterfaceV5.Trade memory openTrade,
      StorageInterfaceV5.OpenLimitOrder memory openLimitOrder
    )
  {
    if (orderType == AggregatorInterfaceV6.OrderType.MARKET_OPEN) {
      StorageInterfaceV5.PendingMarketOrder memory o = storageT.reqID_pendingMarketOrder(orderId);
      openTrade = o.trade;
    } else if (orderType == AggregatorInterfaceV6.OrderType.MARKET_CLOSE) {
      StorageInterfaceV5.PendingMarketOrder memory o = storageT.reqID_pendingMarketOrder(orderId);
      openTrade = storageT.openTrades(o.trade.trader, o.trade.pairIndex, o.trade.index);
    } else if (orderType == AggregatorInterfaceV6.OrderType.UPDATE_SL) {
      AggregatorInterfaceV6 aggregator = storageT.priceAggregator();
      AggregatorInterfaceV6.PendingSl memory o = aggregator.pendingSlOrders(orderId);
      openTrade = storageT.openTrades(o.trader, o.pairIndex, o.index);
    } else if (orderType == AggregatorInterfaceV6.OrderType.LIMIT_CLOSE) {
      StorageInterfaceV5.PendingNftOrder memory o = storageT.reqID_pendingNftOrder(orderId);
      limitOrderType = LimitOrder(uint8(o.orderType));
      openTrade = storageT.openTrades(o.trader, o.pairIndex, o.index);
    } else if (orderType == AggregatorInterfaceV6.OrderType.LIMIT_OPEN) {
      StorageInterfaceV5.PendingNftOrder memory n = storageT.reqID_pendingNftOrder(orderId);
      openLimitOrder = storageT.getOpenLimitOrder(n.trader, n.pairIndex, n.index);
      NftRewardsInterfaceV6.OpenLimitOrderType t = nftRewards.openLimitOrderTypes(n.trader, n.pairIndex, n.index);
      limitOrderType = t == NftRewardsInterfaceV6.OpenLimitOrderType.REVERSAL
        ? LimitOrder.OPEN_REVERSAL
        : LimitOrder.OPEN_MOMENTUM;
    }
  }
}
