// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

import "@openzeppelin/contracts/utils/math/SafeCast.sol";

/// @dev Collection of some commonly used utility function for array of type uint256
library ArrayBytes {
  using SafeCast for int256;
  using SafeCast for uint256;

  /**
   * @dev Returns the index of the given value in the array.
   * Function complexity is O(n).
   * It might cost very high gas for larger arrays.
   *
   * Requirements:
   * - input array must not be empty
   */
  function indexOf(bytes[] storage _array, bytes memory _value) internal view returns (int256) {
    require(_array.length > 0, "ArrayBytes: array should not be empty");
    for (uint256 i = 0; i < _array.length; i++) {
      if (keccak256(_array[i]) == keccak256(_value)) return i.toInt256();
    }
    return -1;
  }

  /**
   * @dev Remove an element from a given index of the array.
   * It changes the order of the array.
   * It mutates the original array.
   * Function complexity is O(1).
   *
   * Requirements:
   * - input array must not be empty
   * - index must not be greater than the array length
   */
  function remove(bytes[] storage _array, uint256 _index) internal {
    require(_array.length > 0, "ArrayBytes: array should not be empty");
    require(_index < _array.length, "ArrayUint256: index should not be greater than array length");
    _array[_index] = _array[_array.length - 1];
    _array.pop();
  }
}
