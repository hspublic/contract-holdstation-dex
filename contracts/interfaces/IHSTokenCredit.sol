// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

interface IHSTokenCredit {
  struct UserRequest {
    uint256 blockTime;
    uint256 amount;
  }
  struct ContributionRequest {
    uint256 blockTime;
    uint256 rewardDistribution;
    uint256 totalContributed;
  }

  function contributeToken(uint256 _amount, address _userAddress) external;

  function distributeExactlyReward(address _userAddress, uint256 _index) external;

  function notifyBalanceChange(address _fromAddress, address _toAddress) external;

  function forceNewEpoch() external;
}
