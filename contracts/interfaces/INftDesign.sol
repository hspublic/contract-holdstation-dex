// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

interface INftDesign {
  function buildTokenURI(uint256 nftType, uint256 tokenId) external pure returns (string memory);
}
