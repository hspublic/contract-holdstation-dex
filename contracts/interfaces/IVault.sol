// SPDX-License-Identifier: MIT
import "./IHSTokenCredit.sol";
pragma solidity 0.8.10;

interface IVault {
  function manager() external view returns (address);

  function admin() external view returns (address);

  function tokenCredit() external view returns (IHSTokenCredit);

  function currentEpoch() external view returns (uint256);

  function currentEpochStart() external view returns (uint256);

  function currentEpochPositiveOpenPnl() external view returns (uint256);

  function updateAccPnlPerTokenUsed(uint256 prevPositiveOpenPnl, uint256 newPositiveOpenPnl) external returns (uint256);

  function sendAssets(uint256 assets, address receiver) external;

  function receiveAssets(uint256 assets, address user) external;

  function distributeReward(uint256 assets) external;

  function currentBalanceUsdc() external view returns (uint256);
}
