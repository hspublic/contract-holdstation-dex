// SPDX-License-Identifier: MIT

pragma solidity 0.8.10;

interface HSStakingInterfaceV6_2 {
  function distributeRewardUsdc(uint256 amount) external;
}
