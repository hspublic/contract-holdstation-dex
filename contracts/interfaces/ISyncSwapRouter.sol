// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

interface ISyncSwapRouter {
  struct SwapStep {
    address pool;
    bytes data;
    address callback;
    bytes callbackData;
  }

  struct SwapPath {
    SwapStep[] steps;
    address tokenIn;
    uint amountIn;
  }

  struct TokenAmount {
    address token;
    uint amount;
  }

  function swap(
    SwapPath[] memory paths,
    uint amountOutMin,
    uint deadline
  ) external payable returns (TokenAmount memory amountOut);

  function wETH() external view returns (address);
}

interface ISyncSwapPool {
  function getAmountOut(address _tokenIn, uint _amountIn, address _sender) external view returns (uint _amountOut);
}
