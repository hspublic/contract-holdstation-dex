// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IERC20Extend is IERC20 {
  function burn(address to, uint256 amount) external;

  function mint(address from, uint256 amount) external;
}
