// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;
import "./IVault.sol";

interface IVaultExt is IVault {
  function previewRedeem(uint256 shares) external view returns (uint256);

  function totalSupply() external view returns (uint256);

  function asset() external view returns (address);

  function balanceOf(address account) external view returns (uint256);
}
