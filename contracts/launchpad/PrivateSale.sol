// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../helpers/TransferHelper.sol";

contract PrivateSale is Initializable, OwnableUpgradeable, ReentrancyGuardUpgradeable {
  uint public allocation;
  uint public hardCapPerUser;
  uint public hardCap;
  uint public minimumStakingAmount;
  uint public totalStaked;
  uint public totalReward;

  uint64 public endTime;
  uint8 public rewardP;
  IERC20 public stakingToken;
  IERC20 public earningToken;

  bool public claimable;
  address[] public stakerAddresses;
  mapping(address => uint) public stakers;

  mapping(address => uint) public userEarn;
  mapping(address => uint) public userReward;
  mapping(address => uint) public indexOfStakerAddress;
  mapping(address => RefererDetail) public refererDetail;
  mapping(address => bool) public isRefunded;

  event Staked(address indexed sender, uint256 amount, uint totalStaked, address referer);
  event Claimed(address indexed sender, uint256 amount);
  event RewardClaimed(address indexed sender, uint256 amount);
  event Refunded(address indexed sender, uint256 amount);
  event Registerreferer(address indexed referer, address indexed referee, uint256 amount);
  event NothingToCalculate();

  struct RefererDetail {
    address referer;
    uint amount;
  }

  function initialize(
    IERC20 _stakingToken,
    IERC20 _earningToken,
    uint _allocation,
    uint _hardCap,
    uint _hardCapPerUser,
    uint64 _endTime
  ) external initializer {
    __Ownable_init();
    __ReentrancyGuard_init();
    stakingToken = _stakingToken;
    earningToken = _earningToken;
    allocation = _allocation;
    hardCap = _hardCap;
    hardCapPerUser = _hardCapPerUser;
    endTime = _endTime;
    minimumStakingAmount = 0.0001 ether;
    rewardP = 2;
  }

  modifier isActive() {
    require(block.timestamp <= endTime, "EXPIRED");
    _;
  }

  modifier isNotActive() {
    require(block.timestamp > endTime, "ACTIVING");
    _;
  }

  modifier isClaimable() {
    require(claimable, "CAN_NOT_CLAIMABLE_YET");
    _;
  }

  function setRewardP(uint8 _rewardP) external onlyOwner {
    rewardP = _rewardP;
  }

  function setClaimable(bool _claimable) external onlyOwner {
    claimable = _claimable;
  }

  function setEndTime(uint64 _endTime) external onlyOwner {
    endTime = _endTime;
  }

  function setAllocation(uint _allocation) external onlyOwner {
    allocation = _allocation;
  }

  function setMinimumStakingAmount(uint _minimumStakingAmount) external onlyOwner {
    minimumStakingAmount = _minimumStakingAmount;
  }

  function setHardcap(uint _hardCap) external onlyOwner {
    hardCap = _hardCap;
  }

  function setHardcapPerUser(uint _hardCapPerUser) external onlyOwner {
    hardCapPerUser = _hardCapPerUser;
  }

  function setStakingToken(IERC20 _stakingToken) external onlyOwner {
    require(totalStaked <= 0, "USER_ALREADY_STAKED");
    stakingToken = _stakingToken;
  }

  function refillNative() external payable onlyOwner {}

  function stake(uint _amount, address _referer) external isActive nonReentrant {
    require(address(stakingToken) != address(0), "STAKE_ERC20_IS_NOT_ACTIVE");
    require(_amount > minimumStakingAmount, "UNDER_MINIMUM");
    _stake(_amount, _referer);
  }

  function stakeByNative(uint _amount, address _referer) external payable isActive nonReentrant {
    require(address(stakingToken) == address(0), "STAKE_NATVE_IS_NOT_ACTIVE");
    require(_amount > minimumStakingAmount, "UNDER_MINIMUM");
    require(_amount == msg.value, "INVALID_AMOUNT");
    _stake(_amount, _referer);
  }

  function _stake(uint _amount, address _referer) private {
    address stakerAddress = _msgSender();
    stakers[stakerAddress] += _amount;
    totalStaked += _amount;
    address refererAddress = address(0);
    if (_referer != stakerAddress) {
      RefererDetail storage refInfo = refererDetail[stakerAddress];
      if (refInfo.referer == address(0) && _referer != address(0)) {
        refInfo.referer = _referer;
        refInfo.amount = _amount;
      } else if (refInfo.referer != address(0)) {
        refInfo.amount += _amount;
      }
      refererAddress = refInfo.referer;
    }

    if (!_isStakerAddressExist(stakerAddress)) {
      _addStakerAddress(stakerAddress);
    }
    emit Staked(_msgSender(), _amount, totalStaked, refererAddress);
  }

  function finalCalculate(address[] memory _stakers, uint[] memory _earnings) external onlyOwner isNotActive {
    require(!claimable, "CLAIMABLE_ACTIVED");
    uint arrLength = _stakers.length;
    require(arrLength == _earnings.length, "INVALID_DATA");
    for (uint i; i < arrLength; ++i) {
      userEarn[_stakers[i]] = _earnings[i];
    }
  }

  function finalCalculateReward(address[] memory _referers, uint[] memory _earnings) external onlyOwner isNotActive {
    require(!claimable, "CLAIMABLE_ACTIVED");
    uint arrLength = _referers.length;
    require(arrLength == _earnings.length, "INVALID_DATA");
    uint tmpTotalReward = 0;
    for (uint i; i < arrLength; ++i) {
      userReward[_referers[i]] = _earnings[i];
      tmpTotalReward += _earnings[i];
    }
    totalReward += tmpTotalReward;
  }

  function resetTotalReward() external onlyOwner isNotActive {
    totalReward = 0;
  }

  function claim() external isNotActive nonReentrant isClaimable {
    address sender = _msgSender();
    uint totalEarn = userEarn[sender];
    if (totalEarn > 0) {
      userEarn[sender] = 0;
      TransferHelper.safeTransfer(address(earningToken), sender, totalEarn);
      emit Claimed(sender, totalEarn);
    }
    _claimRefund(sender, totalEarn);
  }

  function claimReward() external isNotActive nonReentrant isClaimable {
    address sender = _msgSender();
    require(userReward[sender] > 0, "NOTHING_TO_CLAIM");
    uint totalEarn = userReward[sender];
    userReward[sender] = 0;
    if (address(stakingToken) != address(0)) {
      TransferHelper.safeTransfer(address(stakingToken), sender, totalEarn);
    } else {
      TransferHelper.safeTransferETH(sender, totalEarn);
    }
    emit RewardClaimed(sender, totalEarn);
  }

  function _claimRefund(address _stakerAddress, uint _earned) private {
    require(!isRefunded[_stakerAddress], "ALREADY_REFUNDED");

    uint spendAmount = (_earned * hardCap) / allocation;
    if (stakers[_stakerAddress] > spendAmount) {
      uint refundAmount = stakers[_stakerAddress] - spendAmount;
      if (address(stakingToken) != address(0)) {
        TransferHelper.safeTransfer(address(stakingToken), _stakerAddress, refundAmount);
      } else {
        TransferHelper.safeTransferETH(_stakerAddress, refundAmount);
      }
      isRefunded[_stakerAddress] = true;
      emit Refunded(_stakerAddress, refundAmount);
    }
  }

  function deplete(IERC20 _token, address _to) external onlyOwner {
    if (address(_token) != address(0)) {
      _token.transfer(_to, _token.balanceOf(address(this)));
    } else {
      (bool success, ) = payable(_to).call{value: address(this).balance}("");
      require(success, "WITHDRAW_NATIVE_FAIL");
    }
  }

  function getRefundingAmount(address _staker) external view returns (uint) {
    uint spendAmount = (userEarn[_staker] * hardCap) / allocation;
    if (!isRefunded[_staker] && stakers[_staker] > spendAmount) {
      return stakers[_staker] - spendAmount;
    }
    return 0;
  }

  function totalUser() external view returns (uint) {
    return stakerAddresses.length;
  }

  function _addStakerAddress(address _stakerAddress) private {
    stakerAddresses.push(_stakerAddress);
    indexOfStakerAddress[_stakerAddress] = stakerAddresses.length - 1;
  }

  function _isStakerAddressExist(address _stakerAddress) private view returns (bool) {
    uint index = indexOfStakerAddress[_stakerAddress];
    if (stakerAddresses.length == 0 || (index == 0 && stakerAddresses[0] != _stakerAddress)) {
      return false;
    }
    return true;
  }
}
