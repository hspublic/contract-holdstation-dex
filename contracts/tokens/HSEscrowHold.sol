// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;
/// @dev The transfer function is disabled.
error TransferIsDisabled();

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/MathUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/MathUpgradeable.sol";
import "../interfaces/IOpenTradesPnlFeed.sol";
import "../interfaces/IVaultExt.sol";
import {ISyncSwapPool} from "../interfaces/ISyncSwapRouter.sol";
import "../interfaces/IOwnable.sol";
import "../interfaces/IHSTokenCredit.sol";
import "../helpers/ArrayAddress.sol";

contract HSEscrowHold is
  Initializable,
  OwnableUpgradeable,
  AccessControlUpgradeable,
  ERC20Upgradeable,
  ReentrancyGuardUpgradeable,
  IHSTokenCredit
{
  using MathUpgradeable for uint256;
  using ArrayAddress for address[];
  IOpenTradesPnlFeed public openTradesPnlFeed;
  IVaultExt public hToken;
  uint256 public constant PERCENTAGE_BASE = 1e10;
  uint256 public rewardP;
  bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
  ISyncSwapPool public syncSwapPool;

  bool public allowTransfer;
  bool private isMigrated;

  mapping(uint256 => uint256) public totalRewardAtEpoch;

  mapping(uint256 => mapping(address => ContributionRequest)) public userContributedAtEpoch; // epoch=>address=>contributed
  mapping(uint256 => uint256) public totalContributedAtEpoch; //epoch=>contributed
  mapping(address => uint256) public claimedAtEpoch; //user=>epoch

  event Claimed(address indexed user, uint amount, uint atEpoch);

  event BalanceUpdated(address indexed user, uint256 balance, uint256 blockTime, uint256 totalSupply, bool isTransfer);

  event ZeroAmount(address indexed user);
  event InvalidOutAmount(address indexed user, uint requestAmount, uint availableAmount);
  event ForcedEpoch(uint epoch, uint totalSupply, uint rewardP, uint usdcReward, uint holdReward);

  address public weth;
  address public holdToken;
  mapping(address => mapping(address => address)) public syncSwapPools;

  function initialize(
    string memory _name,
    string memory _symbol,
    address _hTokenAddress,
    address _openTradesPnlFeedAddress
  ) external initializer {
    __ERC20_init(_name, _symbol);
    __Ownable_init();
    __AccessControl_init();
    __ReentrancyGuard_init();
    hToken = IVaultExt(_hTokenAddress);
    openTradesPnlFeed = IOpenTradesPnlFeed(_openTradesPnlFeedAddress);
    rewardP = 50 * 1e8; //5%
    _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    _setupRole(MINTER_ROLE, _msgSender());
  }

  // Modifiers
  modifier only(bytes32 role) {
    require(hasRole(role, _msgSender()), "INSUFFICIENT_PERMISSIONS");
    _;
  }

  modifier onlyVault() {
    require(msg.sender == address(hToken), "ONLY_VAULT");
    _;
  }

  modifier onlyOpenPnl() {
    require(msg.sender == address(openTradesPnlFeed), "ONLY_OPEN_PNL");
    _;
  }

  modifier onlyHTokenOwner() {
    require(msg.sender == IOwnable(address(hToken)).owner(), "ONLY_OWNER");
    _;
  }

  // Manage parameters
  function updateOpenPnl(address _openTradesPnlFeedAddress) external onlyHTokenOwner {
    openTradesPnlFeed = IOpenTradesPnlFeed(_openTradesPnlFeedAddress);
  }

  function setAllowTransfer(bool _allowTransfer) public onlyOwner {
    allowTransfer = _allowTransfer;
  }

  function setWETH(address _weth) external onlyOwner {
    weth = _weth;
  }

  function setHoldToken(address _holdToken) external onlyOwner {
    holdToken = _holdToken;
  }

  function setSyncSwapPool(address _token, address _poolAddress) external onlyOwner {
    syncSwapPools[_token][weth] = syncSwapPools[weth][_token] = _poolAddress;
  }

  function claim() external nonReentrant {
    address sender = _msgSender();
    uint currentEpoch = hToken.currentEpoch();
    uint availableToClaim = calculateRewardsEarned(sender);
    uint userBalance = hToken.balanceOf(sender);
    if (userBalance > 0) {
      userContributedAtEpoch[currentEpoch][sender].totalContributed = userBalance;
      userContributedAtEpoch[currentEpoch][sender].blockTime = block.timestamp;
    }
    claimedAtEpoch[sender] = currentEpoch;
    _mint(sender, availableToClaim);
    emit Claimed(sender, availableToClaim, claimedAtEpoch[sender]);
  }

  function burn(address _account, uint256 _amount) external only(MINTER_ROLE) {
    _burn(_account, _amount);
  }

  function mint(address _account, uint256 _amount) external only(MINTER_ROLE) {
    _mint(_account, _amount);
  }

  //deposit / withdraw / transfer / receive transfer
  function notifyBalanceChange(address _fromAddress, address _toAddress) external nonReentrant onlyVault {
    bool isTransfer = _toAddress != address(0) ? true : false;
    uint fromBalance = hToken.balanceOf(_fromAddress);
    uint totalSupply = hToken.totalSupply();
    userContributedAtEpoch[hToken.currentEpoch()][_fromAddress].totalContributed = fromBalance;
    userContributedAtEpoch[hToken.currentEpoch()][_fromAddress].blockTime = block.timestamp;
    totalContributedAtEpoch[hToken.currentEpoch()] = totalSupply;
    emit BalanceUpdated(_fromAddress, fromBalance, block.timestamp, totalSupply, isTransfer);
    if (isTransfer) {
      uint toBalance = hToken.balanceOf(_toAddress);
      userContributedAtEpoch[hToken.currentEpoch()][_toAddress].totalContributed = toBalance;
      userContributedAtEpoch[hToken.currentEpoch()][_toAddress].blockTime = block.timestamp;
      emit BalanceUpdated(_toAddress, toBalance, block.timestamp, totalSupply, isTransfer);
    }
  }

  //run after updateAccPnlPerTokenUsed, so currentEpoch() is new epoch
  function forceNewEpoch() external nonReentrant onlyOpenPnl {
    uint currentEpoch = hToken.currentEpoch();
    uint totalSupply = hToken.totalSupply();
    totalContributedAtEpoch[currentEpoch] = totalSupply;
    uint usdcReward = (hToken.previewRedeem(totalSupply) * rewardP) / PERCENTAGE_BASE;
    totalRewardAtEpoch[currentEpoch - 1] = getHoldRewardValue(usdcReward);
    emit ForcedEpoch(currentEpoch, totalSupply, rewardP, usdcReward, totalRewardAtEpoch[currentEpoch - 1]);
  }

  //call when user deposit/withdraw
  function calculateRewardsEarned(address _userAddress) public view returns (uint availableToClaim) {
    uint fromEpoch = claimedAtEpoch[_userAddress];
    uint256 lastContributed = userContributedAtEpoch[fromEpoch][_userAddress].totalContributed;
    for (uint i = fromEpoch; i < hToken.currentEpoch(); ++i) {
      // Blocktime will be updated when user interact with contract
      // Eg: epoch:1 200, 2: no interact,3 : no interact, 4: +300 => 500
      // availableToClaim at epoch 1,2,3 = 200 * totalRewardEachEpoch/totalContributedAtEpoch(1/2/3)
      // availableToClaim at epoch 4 = 500 * totalRewardEachEpoch/totalContributedAtEpoch(4)
      if (userContributedAtEpoch[i][_userAddress].blockTime > 0) {
        lastContributed = userContributedAtEpoch[i][_userAddress].totalContributed;
      }
      if (totalContributedAtEpoch[i] != 0) {
        availableToClaim += (lastContributed * totalRewardAtEpoch[i]) / totalContributedAtEpoch[i];
      }
    }
  }

  function setRewardP(uint _rewardP) external onlyOwner {
    require(_rewardP <= PERCENTAGE_BASE, "INVALID_INPUT");
    rewardP = _rewardP;
  }

  function migrateData(address[] calldata _users) external onlyOwner {
    require(!isMigrated, "MIGRATED");
    uint currentEpoch = hToken.currentEpoch();
    uint arrLength = _users.length;
    for (uint i; i < arrLength; ++i) {
      claimedAtEpoch[_users[i]] = currentEpoch;
      userContributedAtEpoch[currentEpoch][_users[i]].totalContributed = hToken.balanceOf(_users[i]);
    }
  }

  function migrateTotalContributed() external onlyOwner {
    require(!isMigrated, "MIGRATED");
    uint currentEpoch = hToken.currentEpoch();
    totalContributedAtEpoch[currentEpoch] = hToken.totalSupply();
    isMigrated = true;
  }

  function setSyncSwapPool(ISyncSwapPool _syncSwapPool) external onlyOwner {
    syncSwapPool = _syncSwapPool;
  }

  function _transfer(address from, address to, uint256 amount) internal virtual override {
    if (!allowTransfer) {
      revert TransferIsDisabled();
    }
    super._transfer(from, to, amount);
  }

  function _approve(address owner, address spender, uint256 amount) internal virtual override {
    if (!allowTransfer) {
      revert TransferIsDisabled();
    }
    super._approve(owner, spender, amount);
  }

  function contributeToken(uint256 _amount, address _userAddress) external {
    revert("UNIMPLEMENT");
  }

  function distributeExactlyReward(address _userAddress, uint256 _index) external {
    revert("UNIMPLEMENT");
  }

  function getHoldRewardValue(uint _usdcAsset) public view returns (uint) {
    //convert usdc -> eth
    uint ethAmount = ISyncSwapPool(syncSwapPools[hToken.asset()][weth]).getAmountOut(
      hToken.asset(),
      _usdcAsset,
      address(this)
    );
    //convert eth -> hold
    return ISyncSwapPool(syncSwapPools[holdToken][weth]).getAmountOut(weth, ethAmount, address(this));
  }
}
